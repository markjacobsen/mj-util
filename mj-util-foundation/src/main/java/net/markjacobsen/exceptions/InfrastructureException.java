package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class InfrastructureException extends Exception {
	private static final long serialVersionUID = 1L;

	public InfrastructureException(Throwable exception) {
		super(exception);
	}
	
	public InfrastructureException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public InfrastructureException(String message) {
		super(message);
	}
}