package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class GeneralException extends Exception {
	private static final long serialVersionUID = 1L;

	public GeneralException(Throwable exception) {
		super(exception);
	}
	
	public GeneralException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public GeneralException(String message) {
		super(message);
	}
}