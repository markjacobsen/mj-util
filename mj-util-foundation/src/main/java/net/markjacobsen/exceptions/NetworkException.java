package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class NetworkException extends Exception {
	private static final long serialVersionUID = 1L;

	public NetworkException(Throwable exception) {
		super(exception);
	}
	
	public NetworkException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public NetworkException(String message) {
		super(message);
	}
}