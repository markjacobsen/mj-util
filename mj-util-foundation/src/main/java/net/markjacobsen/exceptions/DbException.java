package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class DbException extends Exception {
	private static final long serialVersionUID = 1L;

	public DbException(Throwable exception) {
		super(exception);
	}
	
	public DbException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public DbException(String message) {
		super(message);
	}
}