package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class FileSystemException extends Exception {
	private static final long serialVersionUID = 1L;

	public FileSystemException(Throwable exception) {
		super(exception);
	}
	
	public FileSystemException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public FileSystemException(String message) {
		super(message);
	}
}