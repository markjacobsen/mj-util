package net.markjacobsen.exceptions;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class ValidationException extends Exception {
	private static final long serialVersionUID = 1L;

	public ValidationException(Throwable exception) {
		super(exception);
	}
	
	public ValidationException(String message, Throwable exception) {
		super(message, exception);
	}
	
	public ValidationException(String message) {
		super(message);
	}
}