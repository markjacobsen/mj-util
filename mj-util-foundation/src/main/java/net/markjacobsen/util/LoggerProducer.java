package net.markjacobsen.util;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
@Dependent
public class LoggerProducer {
	/**
	 * CDI Producer for {@link Logger} objects.  Do not use directly.
	 * 
	 * @param injectionPoint
	 * @return {@link Logger}
	 */
	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		return getLogger(injectionPoint.getMember().getDeclaringClass());
	}
	
	/**
	 * Convenience method to provide a class Logger when CDI isn't available.  The class logger will be named based on the full class name of the clazz parameter.
	 * @param clazz The class to provide a logger for.
	 * @return {@link Logger}
	 */
	public static Logger getLogger(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz);
	}
	
	/**
	 * Convenience method to provide a logger with a given name when CDI isn't available.
	 * @param logger The name of the logger.
	 * @return {@link Logger}
	 */
	public static Logger getLogger(String logger) {
		return LoggerFactory.getLogger(logger);
		
	}
}
