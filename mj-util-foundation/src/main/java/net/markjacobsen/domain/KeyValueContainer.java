package net.markjacobsen.domain;

/**
 * Useful for creating a list of Key/Value pairs, esp when duplicates possible 
 * 
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class KeyValueContainer
{
	private String code;
	private String value;
	
	public KeyValueContainer(String code, String value)
	{
		this.code = code;
		this.value = value;
	}
	
	public String toString() { return this.getCode() + "::" + this.getValue(); }
	public String getCode() { return this.code; }
	public String getValue() { return this.value; }
}
