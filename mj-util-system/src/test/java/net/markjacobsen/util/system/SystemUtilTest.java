package net.markjacobsen.util.system;

import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(WeldJUnit4Runner.class)
public class SystemUtilTest {
	@Inject
	SystemUtil sysUtil;
	
	@Test
	public void testMemory() {
		System.out.println("Free Memory: "+sysUtil.getMemoryFree());
		System.out.println("Used Memory: "+sysUtil.getMemoryUsed());
		System.out.println("Tot Memory: "+sysUtil.getMemoryTotalCurrentHeap());
		System.out.println("Max Heap Memory: "+sysUtil.getMemoryMaxHeap());
		System.out.println("Heap Available: "+sysUtil.getMemoryHeapAvailable());
		System.out.println("Heap Available %: "+sysUtil.getMemoryHeapAvailablePercent());
		System.out.println("Heap Used %: "+sysUtil.getMemoryHeapUsedPercent());
		assertTrue(true);
	}
}
