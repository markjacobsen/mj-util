package net.markjacobsen.util.system;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.markjacobsen.util.system.WeldJUnit4Runner;

@RunWith(WeldJUnit4Runner.class)
public class FileUtilsTest {
	@Inject
	FileUtil fileUtil;
	
	private final String TEST_FILE = "src/test/java/net/markjacobsen/util/system/test.txt"; 
	
	@Test
	public void testGetLastLine() throws IOException {
		File file = new File(TEST_FILE);
		String line = fileUtil.getLastLine(file.getAbsolutePath());
		assertEquals("This is line 3", line);
	}
	
	@Test
	public void testGetLastXLines() throws IOException {
		int linesToRead = 2;
		File file = new File(TEST_FILE);
		List<String> lines = fileUtil.getLastXLines(file.getAbsolutePath(), linesToRead);
		assertEquals(linesToRead, lines.size());
		assertEquals("This is line 3", lines.get(0));
		assertEquals("This is line 2", lines.get(1));
	}
	
	@Test
	public void testGetFirstXLines() throws IOException {
		int linesToRead = 2;
		File file = new File(TEST_FILE);
		List<String> lines = fileUtil.getFirstXLines(file.getAbsolutePath(), linesToRead);
		assertEquals(linesToRead, lines.size());
		assertEquals("This is line 1", lines.get(0));
		assertEquals("This is line 2", lines.get(1));
	}
	
	@Test
	public void testGetFileExtension() {
		assertEquals("txt", fileUtil.getFileExtension("somefile.txt"));
		assertEquals("txt", fileUtil.getFileExtension("somefile.withextradot.txt"));
		assertEquals("", fileUtil.getFileExtension("somefile"));
	}
	
	@Test
	public void testGtLinesFromLastOccurence() throws IOException {
		File file = new File(TEST_FILE);
		assertEquals(2, fileUtil.getLinesFromLastOccurence(file.getAbsolutePath(), "line 2").size());
	}
	
	@Test
	public void testGetFileNameWithoutExtension() {
		assertEquals("test", fileUtil.getFileNameWithoutExtension("utils/file/test.txt"));
		assertEquals("test", fileUtil.getFileNameWithoutExtension("utils/file/test"));
	}
}
