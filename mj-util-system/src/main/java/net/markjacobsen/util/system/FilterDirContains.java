/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util.system;

import java.io.File;
import java.io.FilenameFilter;

public class FilterDirContains implements FilenameFilter {	
	String find;

	FilterDirContains(String find) {
		this.find = find;
	}

	public boolean accept(File dir, String name) {
		boolean accept = false;
		if ((find == null) || ("".equals(find)) || (find.equalsIgnoreCase("*") == true) || (find.equalsIgnoreCase("*.*") == true)) {
			accept = true;
		} else {
			String f = new File(name).getName(); // Strip path information (get just the filename)
			if (f.indexOf(find) != -1) {
				accept = true;
			}
		}
		return accept;
	}
}
