/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util.system;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class WorkUtil {
	@Inject
	Logger logger;
	
	@Inject
	SystemUtil sysUtil;
	
	@Inject
	FileUtil fileUtil;
	
	/**
	 * Get the path to the logged in user home directory
	 * @return Full path to users home directory
	 */
	public String getDirHome()
	{
		String ret = null;
		try
		{
			if (sysUtil.isWindows()){
				String homePath = sysUtil.getEnvVal("HOMEPATH");
				if (homePath.substring(homePath.length() - 1).equalsIgnoreCase("\\") == true)
				{
					// Strip the any trailing \
					homePath = homePath.substring(0, homePath.length() - 1);
				}
				ret = sysUtil.getEnvVal("HOMEDRIVE") + homePath;
			}else{
				ret = sysUtil.getEnvVal("HOME");
			}
		}
		catch (Exception e)
		{
			ret = null;
		}
		
		return ret;
	}
	
	public String getDirDocs() {
		if (sysUtil.isWindows()) {
			return getDirHome() + fileUtil.getPathSeparator() + "My Documents";
		} else {
			return getDirHome();
		}
	}
	
	/**
	 * Get the path to the CFConfig directory (found in users home dir)
	 * @return Full path to CFConfig
	 */
	public String getDirConfig() {
		String dir = getDirHome() + fileUtil.getPathSeparator() + "CFConfig";
		fileUtil.createFolder(dir);
		return dir;
	}
	
	/**
	 * Get the path to the CFWork directory (found in users home dir)
	 * @return Full path to CFWork
	 */
	public String getDirWork() {
		String dir = getDirHome() + fileUtil.getPathSeparator() + "CFWork";
		fileUtil.createFolder(dir);
		return dir;
	}
	
	/**
	 * Get a subdirectory off the working directory being sure to create
	 * each subdir if it does not exist
	 * @param dirs Array of directories off the working dir
	 * @return The full path
	 */
	public String getDirWork(String[] dirs) {
		String dir = getDirWork();
		
		for (int x = 0; x < dirs.length; x++) {
			dir += fileUtil.getPathSeparator() + dirs[x];
			fileUtil.createFolder(dir);
		}
		
		return dir;
	}
	
	public String getDirTemp() {
		String dir = getDirWork() + fileUtil.getPathSeparator() + "temp";
		fileUtil.createFolder(dir);
		return dir;
	}
	
	public String getFileDefaultOutput() {
		String file = getDirWork() + fileUtil.getPathSeparator() + "default.out";
		fileUtil.createFile(file, true);
 		return file;
	}
	
	/**
	 * Run a random command and don't care if it succeeds.  Useful for popping a file in notepad or something
	 * @param command The command to run (ex: "\"C:\\Program Files\\Notepad++\\notepad++.exe\" \"" + file + "\"")
	 */
	public int execIt(String command) {
		int returnVal = -1;
		
		try {
			Process process = Runtime.getRuntime().exec(command);
			returnVal = process.exitValue();
		} catch (Exception e) {}
		
		return returnVal;
	}
	
	public int exec(String command) { return exec(command, null); }
	public int exec(String command, String[] args) { return exec(command, args, null); }
	public int exec(String command, String[] args, String workingDir) { return exec(command, args, workingDir, null); }
	public int exec(String command, String[] args, String workingDir, String outputRedirectFile) { return exec(command, args, workingDir, outputRedirectFile, false); }
	public int exec(String command, String[] args, String workingDir, String outputRedirectFile, boolean returnImmediately) {
		int returnCode = -1;
		File dir = null;
		String[] cmd = null;
		
		try {
			if (fileUtil.folderExists(workingDir)) {
				dir = new File(workingDir);
			}
			
			List<String> commands = new ArrayList<String>();
			if (sysUtil.isWindows() == true) {
				commands.add("cmd.exe");
				commands.add("/C");
				commands.add(command);
			} else {
				String shell = sysUtil.getShellPath();
				if (shell == null) {
					throw new NullPointerException("shell is null");
				}
				logger.trace("Shell path: {}", shell);
				commands.add(shell);
				commands.add(command);
			}
			logger.trace("Command size={}", commands.size());
			cmd = new String[commands.size()];
			for (int x = 0; x < commands.size(); x++) {
				String tmp = commands.get(x);
				if (tmp == null) {
					logger.warn("Item {} is null", x);
				}
				cmd[x] = tmp;
			}
			Process process = Runtime.getRuntime().exec(cmd, args, dir);
			
			if(!returnImmediately) {
				if (outputRedirectFile == null){
					if (fileUtil.folderExists(workingDir) == true){
						outputRedirectFile = fileUtil.buildPath(workingDir, "tmp.out");
					} else {
						outputRedirectFile = getFileDefaultOutput();
					}
				}
				FileOutputStream fos = null;
				if (fileUtil.fileExists(outputRedirectFile) == true){
					logger.trace("Output redirecting to: {}", outputRedirectFile);
					fos = new FileOutputStream(outputRedirectFile);
				}
				StreamGobbler stderr = new StreamGobbler(process.getErrorStream(), "ERROR");
				StreamGobbler stdout = new StreamGobbler(process.getInputStream(), "OUTPUT", fos);
				
				stderr.start();
				stdout.start();
				
				returnCode = process.waitFor();
				logger.debug("ExitValue: " + returnCode);
				if (fos != null) {
					fos.flush();
					fos.close();
				}
			}
		} catch (InterruptedException | NullPointerException | IOException e) {
			returnCode = -1;
			logger.error(e.getClass().getSimpleName() + " in exec of " + command, e);
			e.printStackTrace();
		}
		
		return returnCode;
	}
}
