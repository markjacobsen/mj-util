/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util.system;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import net.markjacobsen.util.Convert;

@ApplicationScoped
public class SystemUtil {
	@Inject
	Logger logger;
	
	@Inject
	FileUtil fileUtil;
	
	public String getUsername() {
		if (isWindows()){
			return getEnvVal("USERNAME");
		}else{
			return getEnvVal("USER");
		}
	}
	
	public String getNewline() {
		if (isWindows()) {
			return "\r\n";
		} else {
			return "\n";
		}
	}
	
	public String getEnvVal(String key) {
		return System.getenv().get(key);
	}
	
	public void sleep(double seconds) {
        try {
            Thread.sleep(Convert.toInt(seconds * 1000));
        } catch (InterruptedException e) {
            logger.error("ERROR: Sleeping");
        }
    }
	
	public void sleep(int seconds) {
        sleep((double)seconds);
    }
	
	/**
	 * Get the *nix shell path 
	 * @return Path to *nix shell (tries SHELL env var first followed by /bin/bash/, 
	 *           /bin/ksh, /usr/bin/bash, /usr/bin/bash2, /usr/bin/ksh)
	 */
	public String getShellPath() {
		String shell = getEnvVal("SHELL");
		if ((shell == null) || (shell.length() == 0)) {
			logger.trace("Trying hard coded list of shells");
			String[] shells = {"/bin/bash", "/bin/ksh", "/usr/bin/bash", "/usr/bin/bash2", "/usr/bin/ksh"};
			for (int x = 0; x < shells.length; x++) {
				shell = shells[x];
				if (fileUtil.fileExists(shell)) {
					logger.trace("{} exists", shell);
					break;
				}
			}
		}

		logger.trace("Shell={}", shell);
		return shell;
	}
	
	public boolean isWindows() {
		if (System.getProperty("os.name").indexOf("Windows") >= 0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @return Free memory in MB
	 */
	public int getMemoryFree() {
		int mb = 1024*1024;
		return Convert.toInt(Runtime.getRuntime().freeMemory() / mb);
	}
	
	/**
	 * @return Used memory in MB
	 */
	public int getMemoryUsed() {
		int mb = 1024*1024;
		Runtime runtime = Runtime.getRuntime();
		return Convert.toInt((runtime.totalMemory() - runtime.freeMemory()) / mb);
	}
	
	/**
	 * @return Total memory (in MB) allocated in the current heap
	 */
	public int getMemoryTotalCurrentHeap() {
		int mb = 1024*1024;
		return Convert.toInt(Runtime.getRuntime().totalMemory() / mb);
	}
	
	/**
	 * @return Heap memory available (in MB) calculated by: maxMemory() - totalMemory()
	 */
	public int getMemoryHeapAvailable() {
		int mb = 1024*1024;
		Runtime runtime = Runtime.getRuntime();
		return Convert.toInt((runtime.maxMemory() - runtime.totalMemory()) / mb);
	}
	
	/**
	 * @return Heap memory available as a percentage
	 */
	public int getMemoryHeapAvailablePercent() {
		int mb = 1024*1024;
		Runtime runtime = Runtime.getRuntime();
		double available = (runtime.maxMemory() - runtime.totalMemory()) / mb;
		double max = (double)runtime.maxMemory() / mb;
		double pct = available / max;
		return Convert.toInt(pct * 100);
	}
	
	/**
	 * @return Heap memory used as a percentage
	 */
	public int getMemoryHeapUsedPercent() {
		return 100 - getMemoryHeapAvailablePercent();
	}
	
	/**
	 * @return Max memory (in MB) allowed before getting OOM
	 */
	public int getMemoryMaxHeap() {
		int mb = 1024*1024;
		return Convert.toInt(Runtime.getRuntime().maxMemory() / mb);
	}
}
