/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util.system;

import java.io.File;
import java.io.FilenameFilter;

public class FilterNameContains implements FilenameFilter {
	String find;
	
	public FilterNameContains(String find) {
		this.find = find.toLowerCase();
	}
	
	public boolean accept(File dir, String name) { 
		return name.toLowerCase().contains(this.find); 
	}
}
