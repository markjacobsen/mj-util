package net.markjacobsen.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class ValidateTest {
	@Test
	public final void isDateTest() {
		assertFalse(Validate.isDate("kajsdfljsdf"));
		assertFalse(Validate.isDate("13/02/2015"));
		assertTrue(Validate.isDate("01/02/2015"));
		assertTrue(Validate.isDate("12/31/9999"));
		assertTrue(Validate.isDate("01/01/1900"));
		assertTrue(Validate.isDate("2015-01-05"));
		assertTrue(Validate.isDate("2015-01-05 23:45:15"));
		assertFalse(Validate.isDate("2015-01-40 23:45:15"));
		assertFalse(Validate.isDate("2015-01-05 99:45:15"));
	}
	
	@Test
	public final void isPhoneNumberTest() {
		assertTrue(Validate.isPhoneNumber("234-456-4987"));
		assertTrue(Validate.isPhoneNumber("(234) 456-4987"));
		assertTrue(Validate.isPhoneNumber("234.456.4987"));
		assertTrue(Validate.isPhoneNumber("+1 234-456-4987"));
		
		assertFalse(Validate.isPhoneNumber("234-456-498"));
		assertFalse(Validate.isPhoneNumber("234-G56-4987"));
		assertFalse(Validate.isPhoneNumber("456-4987"));
	}
	
	@Test
	public final void isEmailTest() {
		assertTrue(Validate.isEmail("me@a.com"));
		assertTrue(Validate.isEmail("something@cfg2.com"));
		assertTrue(Validate.isEmail("joe.heisman@there.net"));
		
		assertFalse(Validate.isEmail("mark@cfg2"));
	}
	
	@Test
	public final void isIntTest() {
		assertTrue(Validate.isInt("1,800.23", true));
		assertTrue(Validate.isInt("1,800", true));
		assertTrue(Validate.isInt("1800.23", true));
		assertTrue(Validate.isInt("1800", true));
		assertTrue(Validate.isInt("1800", false));
		assertFalse(Validate.isInt("1,800.23", false));
		assertFalse(Validate.isInt("1,800", false));
		assertFalse(Validate.isInt("1800.23", false));
	}
	
	@Test
	public final void isIntListTest() {
		assertTrue(Validate.isIntList("12, 15,9", ",", true));
		assertTrue(Validate.isIntList("1", ",", true));
		
		assertFalse(Validate.isIntList("12, 15,9", ",", false));
	}
}
