package net.markjacobsen.util;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(WeldJUnit4Runner.class)
public class MiscUtilTest {
	@Inject
	MiscUtil miscUtil;
	
	@Test
	public void replaceLastTest()
	{
		String str = "This is my test string";
		String actual = miscUtil.replaceLast(str, "test", "favorite");
		String expected = "This is my favorite string";
		assertEquals(expected, actual);
		
		actual = miscUtil.replaceLast(str, "string", "widget");
		expected = "This is my test widget";
		assertEquals(expected, actual);
		
		actual = miscUtil.replaceLast(str, "asdlfaslkf", "widget");
		expected = "This is my test string";
		assertEquals(expected, actual);
	}
}
