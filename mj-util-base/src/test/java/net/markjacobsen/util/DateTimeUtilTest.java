package net.markjacobsen.util;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(WeldJUnit4Runner.class)
public class DateTimeUtilTest {
	Logger LOG = LoggerFactory.getLogger(DateTimeUtilTest.class);
	
	@Inject
	DateTimeUtil dtUtil;
	
	@Test
	public void testDayOfYear() throws IOException {
		Date date = Convert.toDate("01/02/2012");
		int doy = dtUtil.dayOfYear(date);
		assertEquals(2, doy);
		
		date = Convert.toDate("02/02/2012");
		doy = dtUtil.dayOfYear(date);
		assertEquals(33, doy);
	}

	@Test
	public void testMonth() {
		assertEquals(dtUtil.month("January"), 1);
		assertEquals(dtUtil.month("feb"), 2);
		assertEquals(dtUtil.month("lkasdflj"), -1);
	}
	
	@Test
	public void testTimePieces() {
		Calendar test = dtUtil.setDateTime(2015, 10, 3, 15, 22, 16, 0);
		assertEquals(dtUtil.hour(test), 3);
		assertEquals(dtUtil.hour24(test), 15);
		assertEquals(dtUtil.minute(test), 22);
		assertEquals(dtUtil.second(test), 16);
	}
	
	@Test
	public void testSetValues() {
		Calendar today = Calendar.getInstance();
		Calendar noTime = dtUtil.stripTime(today);
		//LOG.debug("{}-{}", today.get(Calendar.MONTH), noTime.get(Calendar.MONTH));
		assertEquals(today.get(Calendar.MONTH), noTime.get(Calendar.MONTH));
	}
	
	@Test
	public void testEquality() {
		Calendar one = dtUtil.setDateTime(2015, 10, 3, 5, 23, 9, 2);
		Calendar two = dtUtil.setDateTime(2015, 10, 7, 15, 32, 17, 2);
		assertTrue(dtUtil.datesEqual(dtUtil.dateAdd(one, 4, DatePart.DAY), two, true));
		assertFalse(dtUtil.datesEqual(dtUtil.dateAdd(one, 4, DatePart.DAY), two, false));
	}
	
	/*
	@Test
	public void testStandardizeDate() {
		Calendar one = dtUtil.setDateTime(2015, 10, 3, 5, 23, 9);
		Calendar two = dtUtil.standardizeDate("10/03/2015 05:23:09");
		assertTrue(dtUtil.datesEqual(one, two, false));
		
		two = dtUtil.standardizeDate("2015-10-03 05:23:09");
		assertTrue(dtUtil.datesEqual(one, two, false));
		
		two = dtUtil.standardizeDate("2015/10/03");
		assertTrue(dtUtil.datesEqual(one, two, true));
		
		two = dtUtil.standardizeDate("2015-10-03");
		assertTrue(dtUtil.datesEqual(one, two, true));
		
		two = dtUtil.standardizeDate("10/03/2015");
		assertTrue(dtUtil.datesEqual(one, two, true));
		
		two = dtUtil.standardizeDate("2015/10/03");
		assertTrue(dtUtil.datesEqual(one, two, true));
	}
	*/
}
