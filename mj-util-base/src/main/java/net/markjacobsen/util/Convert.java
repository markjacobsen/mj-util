/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class Convert {
	static Logger logger = LoggerProducer.getLogger(Convert.class);
		
	public static String toBase64(String val) {
		return new String(Base64.encodeBase64(val.getBytes()));
	}
	
	public static String toSHA512(String val, String salt) throws NoSuchAlgorithmException {
	    MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes());
        byte[] bytes = md.digest(val.getBytes());
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}

	public static String toMd5(String val) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(val.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		logger.trace("Digest(in hex format):: {}", sb.toString());

		// convert the byte to hex format method 2
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}

		return hexString.toString();
	}
	
	public static String toDelimitedString(String[] vals, String delimiter) {
		String ret = "";
		for (String val : vals) {
			ret += val + delimiter;
		}
		return ret.substring(0, ret.length() - 1);
	}
	
	public static String toDelimitedString(Set<String> vals, String delimiter) {
		String ret = "";
		for (String val : vals) {
			ret += val + delimiter;
		}
		return ret.substring(0, ret.length() - 1);
	}

	public static String toString(Long val) {
		return String.valueOf(val.longValue());
	}
	
	public static String toString(int val) {
        return (new Integer(val)).toString();
    }
	
	public static String toString(byte val) {
		return Byte.toString(val);
	}
	
	public static String toString(byte[] val) {
		return new String(val);
	}
	
	/**
	 * Return val as a string (lower case)
	 * @param val
	 * @return
	 */
	public static String toString(boolean val) {
		if (val) { 
			return "true"; 
		} else { 
			return "false";
		}
	}
	
	public static String toString(char val) {
		return Character.toString(val);
	}
	
	public static String toString(char[] val) {
		return new String(val);
	}
	
	public static String toString(InputStream val) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(val));
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}
	
	/**
	 * Return a delimited string list of values
	 * @param vals
	 * @return
	 */
	public static String toStringList(List<String> vals, String delimiter) {
		if (vals == null) { return null; }
		
		String ret = "";
		
		if (vals.size() > 0) {
			for (String val : vals) {
				ret += val + delimiter;
			}
			
			return ret.substring(0, ret.length() - delimiter.length());
		} else {
			return "";
		}
	}
	
	/**
	 * Return an array of Strings as a List of Strings. Note: if value passed in is null, an empty List is returned.
	 * @param vals
	 * @return
	 */
	public static List<String> toListOfStrings(String[] vals) {
		List<String> ret = new ArrayList<>();
		if (vals != null) {
			ret = Arrays.asList(vals);
		}
		return ret;
	}
	
	/**
	 * Takes a delimited list of strings and returns the elements as a List of Strings
	 * @param val
	 * @param regexDelimiter
	 * @param trimWhiteSpace
	 * @return
	 */
	public static List<String> toListOfStrings(String val, String regexDelimiter, boolean trimWhiteSpace) {
		String[] vals = null;
		if (val != null) {
			vals = val.split(regexDelimiter);
			if (trimWhiteSpace == true) {
				for (int x = 0; x < vals.length; x++) {
					vals[x] = vals[x].trim();
				}
			}
		}
		return toListOfStrings(vals);
	}
	
	//------------------------------------------------------------------
	// Int methods
	@Deprecated
	public static int toInt(String val) {
		return toInt(val, true);
	}
	
	public static int toInt(String val, boolean liberalParse) {
		try {
			if (liberalParse == true) {
				return NumberFormat.getNumberInstance(Locale.getDefault()).parse(val).intValue();
			} else {
				return (new Integer(val));
			}
		} catch (ParseException e) {
			throw new NumberFormatException(val+" is not a valid integer");
		}
	}
	
	public static int toInt(long val) {
		return (new Long(val)).intValue();	
	}
	
	public static int toInt(double val) {
		return (new Double(val)).intValue();	
	}
	
	public static int toInt(boolean val) {
	    if (val) {
	        return 1;
	    } else {
	        return 0;
	    }
	}
	
	public static int[] toIntArray(String[] vals) {
		int[] retArray = new int[vals.length];
		for (int x = 0; x < vals.length; x++) {
			retArray[x] = toInt(vals[x]);
		}
		return retArray;	
	}

	public static Integer toInteger(int val) {
		return new Integer(val);
	}
	
	//------------------------------------------------------------------
	// Double methods
	public static double toDouble(String val) {
		try {
			return NumberFormat.getNumberInstance(Locale.getDefault()).parse(val).doubleValue();
		} catch (ParseException e) {
			throw new NumberFormatException(val+" is not a valid double");
		}
	}
	
	//------------------------------------------------------------------
    // Boolean methods
	public static boolean toBoolean(int val) {
		if (val == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public static boolean toBoolean(String val) {
		if  (
			(val == null) ||
			(val.trim().length() == 0) ||
			(val.substring(0, 1).equalsIgnoreCase("0")) ||  // Note: that's a zero
			(val.substring(0, 1).equalsIgnoreCase("N")) ||
			(val.substring(0, 1).equalsIgnoreCase("F"))
			)
		{
			return false;
		} else {
			return true;
		}
	}
	
	//------------------------------------------------------------------
    // Long methods
	public static long toLong(String val) {
		try {
			return NumberFormat.getNumberInstance(Locale.getDefault()).parse(val).longValue();
		} catch (ParseException e) {
			throw new NumberFormatException(val+" is not a valid long");
		}
	}
	
	public static long toLong(int val) {
		return (new Long(val)).longValue();
	}
	
    //------------------------------------------------------------------
    // Calendar methods
	public static Calendar toCalendar(String val) {
		// Order to try and standardize dates
        final String[] STD_DATE_FORMATS = new String[] {
        		FormatDate.DEFAULT.getFormat(),
        		"yyyy/MM/dd HH:mm:ss",
            	FormatDate.TIMESTAMP.getFormat(),
            	"MM-dd-yyyy HH:mm:ss",
            	"yyyy/MM/dd-HH:mm:ss",
            	"yyyy-MM-dd-HH:mm:ss",
            	"MM-dd-yyyy-HH:mm:ss",
            	"MM/dd/yyyy-HH:mm:ss",
                "yyyy/MM/dd",
            	FormatDate.FILE.getFormat(),
            	"MM-dd-yyyy",
            	FormatDate.TIMESTAMP.getFormat(),
            	"MMddyyyy"
            };
        
        Calendar result = null;
  	
    	// search through potential date formats to find a match
    	for(String potentialFormatMatch : STD_DATE_FORMATS) {
    		DateFormat source = new SimpleDateFormat(potentialFormatMatch);
    		source.setLenient(false);
    		try {
    			ParsePosition pos = new ParsePosition(0);
    			// no exception is being thrown by this parse method; instead null is returned
    			result = Convert.toCalendar(source.parse(val, pos));
    			
    			if (result == null) {
    				 throw new ParseException("Unparseable date: \"" + val + "\" for \"" + potentialFormatMatch + "\"" , pos.getErrorIndex());
    			} else if (pos.getIndex() != val.length()) {
    				result = null;
    			} 
    			
    			if (result != null) {
    				// Date parsed successfully so we're good
    				break;
    			}
    		} catch (ParseException ex) {
    			// probably just not a matching date format, continue to next possible match
    			continue;
    		}
    	}
    	
    	if (result == null) {
    		logger.warn("Unable to parse into date: "+val);
    	}
    	
    	return result;
	}
	
    public static Calendar toCalendar(String val, String mask) throws ParseException {
    	if (val == null) return null;
    	try {
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(toDate(val, mask));
	        return cal;
    	} catch (Exception e) { logger.error("Not a date {}", val); return null; }
    }
   
    public static Calendar toCalendar(java.util.Date val) {
    	if (val == null) return null;
    	try {
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(val);
	        return cal;
    	} catch (Exception e) { logger.error("Not a date {}", val); return null; }
    }
    
    public static Calendar toCalendar(long val) {
    	Calendar tmp = Calendar.getInstance();
    	tmp.setTimeInMillis(val);
    	return tmp;
    }
   
    //------------------------------------------------------------------
    // Date/Time methods           
    public static java.util.Date toDate(Calendar val) {
    	if (val == null) return null;
        try {
            return val.getTime();
        } catch (Exception e) { logger.error("Not a date {}", val); return null; }
    }
   
    public static java.util.Date toDate(java.sql.Date val) {
    	if (val == null) return null;
        try {
            return (java.util.Date)val;
        } catch (Exception e) { logger.error("Not a date {}", val); return null; }
    }
   
    public static java.util.Date toDate(String val) {
    	return toDate(val, FormatDate.DEFAULT.getFormat());
    }
    
    public static java.util.Date toDate(long val) {
    	return new Date(val);
    }
   
    public static java.util.Date toDate(String val, String mask) {
        String retVal = val;
       
        try {
        	if (mask.compareTo(FormatDate.DEFAULT.getFormat()) == 0) {
        		String[] parts = val.split("/"); // split(a_sVal, "/");
                if ( (parts.length == 3) && (parts[2].length() != 4) ) {
                	String year = toString(Calendar.getInstance().get(Calendar.YEAR));
                    year = year.substring(0, 4 - parts[2].length());
                    year = year + parts[2];
                    retVal = parts[0] + "/" + parts[1] + "/" + year;
                }
            } else if (mask.compareTo(FormatDate.FILE_TIMESTAMP.getFormat()) == 0) {
        		String tmp = val.substring(5, val.length()) + "-" + val.substring(0, 4);
        		retVal = tmp.replace('-', '/');
        		mask = FormatDate.DEFAULT.getFormat(); // Have to reset it to parse correctly
        	} else if (mask.compareTo(FormatDate.TIMESTAMP.getFormat()) == 0) {
        		String tmp = val.substring(5, 10) + "-" + val.substring(0, 4) + " " + val.substring(11, val.length());
        		retVal = tmp.replace('-', '/');
        		mask = "MM/dd/yyyy HH:mm:ss"; // Have to use to parse correctly
        	}
                   
            DateFormat df = new SimpleDateFormat(mask);
            df.setLenient(false);
            return df.parse(retVal);
        }
        catch (Exception e) { return null; }
    }
   
    public static java.util.Date[] toDateArray(String[] vals) throws ParseException {
        return toDateArray(vals, FormatDate.DEFAULT.getFormat());
    }
               
    public static java.util.Date[] toDateArray(String[] vals, String mask) throws ParseException {
        java.util.Date[] dateArray = new java.util.Date[vals.length];
        for (int x = 0; x < vals.length; x++) {
        	dateArray[x] = toDate(vals[x], mask);
        }
        return dateArray;         
    }
    
    public static String toSqlStringList(String list, String delimiter, boolean trimElements) {
    	String ret = "";
    	if (StringUtils.isNotEmpty(list)) {
    		String[] strArray = list.split(delimiter);
    		for (String tmp : strArray) {
    			if (trimElements) {
    				tmp = tmp.trim();
    			}
    			if (ret.length() > 0) {
    				ret += ",";
    			}
    			ret += "'"+tmp+"'";
    		}
    	}
    	return ret;
    }
   
    @SuppressWarnings("deprecation")
	public static java.sql.Date toSqlDate(String val) {
        try {
            return new java.sql.Date(java.sql.Date.parse(val));
        } catch (Exception e) { return null; }
    }
   
    public static java.sql.Date toSqlDate(java.util.Date val) {
        try {
            return (java.sql.Date)val;
        } catch (Exception e) { return null; }
    }
    
    public static java.sql.Date toSqlDate(java.util.Calendar val) {
        try {
            return new java.sql.Date(val.getTimeInMillis());
        } catch (Exception e) { return null; }
    }
   
    public static java.util.Date toTime(String val) throws Exception {
        try {
        	DateFormat df = new SimpleDateFormat(FormatDate.TIME_12_HOUR.getFormat());
            return df.parse(val);
        } catch (Exception e) { return null; }
    }
   
    /***
    * This function converts a standard java.util.Date to a
     * DB2 Formated date.
     * @param inDate java.util.Date to convert to DB2 date string
    * @return DB2 date string
    */
    public static String toDB2DateString(java.util.Date val) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(val);
    }
   
    /***
    * This function converts a standard java.sql.Date to a
     * DB2 Formated date.
     * @param inDate java.sql.Date to convert to DB2 date string
    * @return DB2 date string
    */       
    public static String toDB2DateString(java.sql.Date val) {
    	return val.toString();
    }
    
    /***
    * This function converts a standard java.util.Date to a
     * DB2 Formated date.
     * @param inDate java.util.Date to convert to DB2 date string
    * @return DB2 date string
    * @throws ParseException
    */
    public static java.sql.Date toDB2Date(String val) throws ParseException {
        DateFormat df;
        if (val.trim().charAt(4)=='-'){
            df = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            df = new SimpleDateFormat("MM/dd/yyyy");
        }
       
        java.util.Date tempdate = df.parse(val);
        java.sql.Date db2date = (java.sql.Date)tempdate;

        return db2date;
    }
   
    /**
    * This function converts a standard java.util.Date to a
    * java.sql.Timestamp suitable for a db TIMESTAMP or DATETIME
    * @param a_dVal java.util.Date to convert to a Timestamp object
    * @return java.sql.Timestamp object
    */
    public static java.sql.Timestamp toTimestamp(java.util.Date val) {
    	if (val == null) {
    		return null;
    	} else {
    		return new java.sql.Timestamp(val.getTime());
    	}
    }
    
    public static java.sql.Timestamp toTimestamp(java.util.Calendar val) {
    	return toTimestamp(toDate(val));
    }
    
    public static BigDecimal toBigDecimal(String val) {
    	DecimalFormat df = (DecimalFormat)NumberFormat.getInstance(Locale.getDefault());
    	df.setParseBigDecimal(true);
    	try {
			return ((BigDecimal)df.parseObject(val));
		} catch (ParseException e) {
			throw new NumberFormatException(val+" is not a valid BigDecimal");
		}
    }
    
    public static BigDecimal toBigDecimal(int val) {
    	return new BigDecimal(val);
    }
    
    public static BigDecimal toBigDecimal(long val) {
    	return new BigDecimal(val);
    }
    
    public static BigDecimal toBigDecimal(double val) {
    	return BigDecimal.valueOf(val);
    }
    
    public static BigDecimal toBigDecimalFromCents(long cents) {
    	return toBigDecimal(cents).divide(toBigDecimal(100));
    }
    
    public static BigInteger toBigInteger(long val) {
    	return new BigInteger(toString(val));
    }
    
    public static BigInteger toBigInteger(int val) {
    	return new BigInteger(toString(val));
    }
    
    public static BigInteger toBigInteger(String val) {
    	DecimalFormat df = (DecimalFormat)NumberFormat.getInstance(Locale.getDefault());
    	df.setParseBigDecimal(true);
    	try {
			return ((BigDecimal)df.parseObject(val)).toBigInteger();
		} catch (ParseException e) {
			throw new NumberFormatException(val+" is not a valid BigInteger");
		}
    }

    public static int toCents(BigDecimal dollarAmt) {
		return dollarAmt.movePointRight(2).intValue();
	}
    
    public static int toCents(double dollarAmount) {
    	return toInt(dollarAmount * 100);
    }
    
    public static int toCents(float dollarAmount) {
    	return toInt(dollarAmount * 100);
    }
    
    /**
     * Right now just replaces line breaks with <br/> tags
     * @param text
     * @return
     */
    public static String toHtml(String text) {
    	return text.replaceAll("(\r\n|\n)", "<br />").replace("\\'", "'");
    }
}