/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util;

public enum FormatPhone {
	TEN,
	DOT,
	DASH,
	INT;
}
