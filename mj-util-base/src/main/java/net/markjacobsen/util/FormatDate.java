/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 * 
 * Ref: https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
 */
package net.markjacobsen.util;

public enum FormatDate {
	DEFAULT("MM/dd/yyyy"),
    HUMAN("MM/dd/yyyy hh:mm a"),
    TIMESTAMP("yyyy-MM-dd HH:mm:ss"),
    XML_TIMESTAMP("yyyy-MM-dd'T'HH:mm:ss"),
    YYYYMM("yyyyMM"),
    YYYYMMDD("yyyyMMdd"),
    YYYYMMDDHHMMSS("yyyyMMddHHmmss"),
    TIME_12_HOUR("h:mm a"),
    TIME_24_HOUR("H:mm"),
    FILE("yyyy-MM-dd"),
    FILE_TIMESTAMP("yyyy-MM-dd_HH-mm-ss"),
    DB2_TIMESTAMP("yyyy-MM-dd HH:mm:ss"),
    MMDDYY("MMddyy");
	
	private final String format;
	
	FormatDate(String format) {
		this.format = format;
	}
	
	public String getFormat() {
		return format;
	}
}
