/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 * 
 * Ref: https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
 */
package net.markjacobsen.util;

public enum DatePart {
	SECOND("s"),
	MINUTE("m"),
	HOUR("k"),
	DAY("d"),
	MONTH("M"),
	YEAR("y");
	
	private final String code;
	
	DatePart(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
}
