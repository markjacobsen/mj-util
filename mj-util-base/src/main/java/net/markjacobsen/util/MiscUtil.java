/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import net.markjacobsen.exceptions.InfrastructureException;

@ApplicationScoped
public class MiscUtil {
	@Inject
	Logger logger;
	
	private final String ALPHA_NUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
    /**
     * This is really just a wrapper to System.out.println and System.out.print - just shorter Utils.output("something")
     * @param val What to output
     * @param newline Include a newline at the end of the output
     */
    public void output(String val, boolean newline) {
    	if (newline) {
    		System.out.println(val);
    	}else{
    		System.out.print(val);
    	}
    }
    public void output(String val) { output(val, true); }
    
    /**
     * Convenience method for prompting for input without any prompt shown on screen.
     * Useful for continuation of input until a special character is read.
     * @return The value the user types in
     */
    public String promptBare() { return prompt(null, null, false); }
    
    /**
     * @param prompt What to prompt the user with (ex: "Username:")
     * @param defaultVal The value to use if the user just presses enter (ex: "Username [jdoe]:")
     * @param isPassword If true, don't show the characters the user types
     * @return The value the user types in
     */
    public String prompt(String prompt, String defaultVal, boolean isPassword) {
    	String enteredVal = null;
    	
    	if (prompt == null) {
    		prompt = "";
    	} else {
	    	if (defaultVal != null) { prompt += " [" + defaultVal + "]"; }
			prompt += ": ";
    	}
    	
		// When running inside of Eclipse you won't be able to get access to the Console object
		if (System.console() != null) {
			logger.trace("Got a console object");
			if (isPassword) {
				enteredVal = new String(System.console().readPassword(prompt));
			} else {
				enteredVal = System.console().readLine(prompt);
			}
		} else {
			System.out.print(prompt);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            try {
            	enteredVal = bufferedReader.readLine();
            } catch (Exception e) {
            	enteredVal = null;
            }
		}
		
		if ((enteredVal == null) || (enteredVal.trim().length() == 0)) {
			enteredVal = defaultVal;
		}
		
		return enteredVal;
	}
    public String prompt(String prompt) { return prompt(prompt, null); }
    public String prompt(String prompt, String defaultVal) { return prompt(prompt, defaultVal, false); }
    
    /**
     * @param val String to get the last character from.
     * @return The last character (as a String) in the passed in string.
     */
    public String lastChar(String val) {
    	if (val.length() > 0) {
    		return Character.toString(val.charAt(val.length() - 1));
    	} else {
    		return "";
    	}
    }
    
    /**
     * Return the length of the longest String in the list
     * @param vals
     * @return Length of longest, or -1 if vals or all values in vals are null
     */
    public int longestString(List<String> vals) {
    	int longest = -1;
    	if (vals != null) {
	    	for (String val : vals) {
	    		if ((val != null) && (val.length() > longest)) {
	    			longest = val.length();
	    		}
	    	}
    	}
    	return longest;
    }

    /**
     * Return the length of the longest String in the array
     * @param vals
     * @return Length of longest, or -1 if vals or all values in vals are null
     */
	public int longestString(String[] vals) {
		if (vals == null) {
			return -1;
		} else {
			return longestString(Arrays.asList(vals));
		}
	}
	
	public String[] appendToStringArray(String[] array, String val) {
		String[] newArray = null;
		
		if (array == null) {
			newArray = new String[1];
			newArray[0] = val;
		} else {
			newArray = Arrays.copyOf(array, array.length + 1);
			newArray[newArray.length - 1] = val;
		}
		
		return newArray;
	}
	
	public int[] appendToIntArray(int[] array, int val) {
		int[] newArray = null;
		
		if (array == null) {
			newArray = new int[1];
			newArray[0] = val;
		} else {
			newArray = Arrays.copyOf(array, array.length + 1);
			newArray[newArray.length - 1] = val;
		}
		
		return newArray;
	}
	
	public String getRandomString(int len) {
		Random random = new Random();
		String ret = "";
		for (int x = 0; x < len; x++) {
			ret += ALPHA_NUM.charAt(random.nextInt(ALPHA_NUM.length()));
		}
		return ret;
	}
	
	/**
	 * Replace the last occurrence of a string
	 * @param string String containing the value
	 * @param from Value to find
	 * @param to Value to change find to
	 * @return
	 */
	public String replaceLast(String string, String from, String to)  {
	    int lastIndex = string.lastIndexOf(from);
	    if (lastIndex < 0) { 
	    	 return string;
	    } else {
	    	String tail = string.substring(lastIndex).replaceFirst(from, to);
	    	return string.substring(0, lastIndex) + tail;
	    }
	}
	
	/**
	 * Allows you to get properties from a file on the file system or in the classpath. 
	 * Will use file system first. 
	 * @param file Full path to a properties file or the name of a properties file to pull off the classpath
	 * @return The properties from the file
	 * @throws InfrastructureException 
	 */
	public Properties getProperties(String file) throws InfrastructureException {
		InputStream inputStream = null;
		Properties props = new Properties();
		
		try {
			File tmp = new File(file);
			if (tmp.exists() && tmp.isFile()) {
				logger.info("Loading from passed in file: {}", file);
				inputStream = new FileInputStream(file);
			} else {
				logger.info("Attempting to find file on classpath: {}", file);
				inputStream = MiscUtil.class.getClassLoader().getResourceAsStream(file);
			}
			
			if (inputStream == null) {
				throw new InfrastructureException("Properties file not found: "+file);
			} else {
				logger.debug("Loading property file");
				
				props.load(inputStream);
				inputStream.close();
			}
		} catch (IOException e) {
			throw new InfrastructureException("Error getting properties from: "+file, e);
		}
		
		return props;
	}
	
	/**
	 * If val is null, return defaultVal. If not null, just return val
	 * @param val The value to check for null
	 * @param defaultVal The value to replace val with if it is null
	 * @return
	 */
	public String replaceNull(String val, String defaultVal) {
		if (val == null) {
			val = defaultVal;
		}
		return val;
	}
	
	/**
	 * If val is null, return defaultVal. If not null, just return val
	 * @param val The value to check for null
	 * @param defaultVal The value to replace val with if it is null
	 * @return
	 */
	public BigDecimal replaceNull(BigDecimal val, BigDecimal defaultVal) {
		if (val == null) {
			val = defaultVal;
		}
		return val;
	}
	
	/**
	 * If val is null, return defaultVal. If not null, just return val
	 * @param val The value to check for null
	 * @param defaultVal The value to replace val with if it is null
	 * @return
	 */
	public Integer replaceNull(Integer val, Integer defaultVal) {
		if (val == null) {
			val = defaultVal;
		}
		return val;
	}
}