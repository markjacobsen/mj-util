package net.markjacobsen.util;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

public class Validate {
	static Logger logger = LoggerProducer.getLogger(Validate.class);
	
	@Deprecated
	public static boolean isInt(String val) {
		return isInt(val, true);
	}
	
	/**
	 * Check if a string is an integer
	 * @param val Value to check
	 * @param liberalParse true to consider thousands separators ok, false to ensure strict parsing (i.e. 1056 but not 1,056)
	 * @return
	 */
	public static boolean isInt(String val, boolean liberalParse) {
		boolean is = false;
    	if (hasLength(val) == true) {
			try {
				Convert.toInt(val, liberalParse);
				is = true;
			}
			catch (Exception e){}
    	}
    	return is;
	}
	
	/**
	 * Determine if a string is a list of integers
	 * @param val
	 * @param delimiter
	 * @param trimElements
	 * @return
	 */
	public static boolean isIntList(String val, String delimiter, boolean trimElements) {
		boolean is = false;
		if (hasLength(val)) {
			try {
				String[] strArray = val.split(delimiter);
				
				if (strArray.length > 0) {
					boolean allInts = true;
					for (String tmp : strArray) {
						if (trimElements) {
							tmp = tmp.trim();
						}
						if (!isInt(tmp)) {
							allInts = false;
							break;
						}
					}
					is = allInts;
				}
			} catch (Exception e) {}
		}
		return is;
	}
	
	/**
     * @param val The number to evaluate
     * @return True if all characters in the string are digits, otherwise false
     */
    public static boolean isNumeric(String val) {
        if (!hasLength(val)) {
            return false;
        }
       
        for (int x = 0; x < val.length(); x++) {
            if (Character.isDigit(val.charAt(x)) == false) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * @param val The value to evaluate
     * @return true if a phone number, false otherwise
     */
    public static boolean isPhoneNumber(String val) {
    	boolean result = false;
    	if (hasLength(val)) {
	    	String tmp = Format.phoneNumber(FormatPhone.INT, val);
	    	String numbersOnly = Format.stripNonNumeric(tmp);
	    	if ((tmp.startsWith("+") == true) && (tmp.length() >= 12) && (numbersOnly.length() >= 11)) {
	    		result = true;
	    	}
    	}
    	return result;
    }
   
    /**
     * @param val The date to evaluate
     * @return True if we can convert it to a date, otherwise false
     */
    public static boolean isDate(String val) {
    	boolean isDate = false;
    	if (hasLength(val) == true) {
	        try {
	        	if (Convert.toCalendar(val) != null) {
	            	isDate = true;
	            }
	        } catch (Exception e) {}
    	}
        return isDate;
    }
    
    /**
     * @param val The value to evaluate
     * @return true if an email address, false otherwise
     */
    public static boolean isEmail(String val) {
    	boolean result = false;
    	if 	(
    		(val.length() > 7) && 
    		val.contains("@") && 
    		val.contains(".") &&
			(val.indexOf("@") < val.lastIndexOf("."))
			) {
			result = true;
		}
    	return result;
    }
    
    /**
     * Determine if a string has length and is not null (Does not trim)
     * @param val The value to evaluate
     * @return True if it is not null nor a ZLS, false otherwise
     */
    public static boolean hasLength(String val) {
    	return StringUtils.isNotEmpty(val);
    }
    
    /**
     * Determine if a collection contains elements and is not null
     * @param val The collection to evaluate
     * @return True if it is not null and has contains 1 or more elements, false otherwise
     */
    public static boolean hasSize(Collection<?> val) {
    	boolean hasSize = false;
    	try {
    		if ((val != null) && (val.isEmpty() == false)){
    			hasSize = true;
    		}
    	} catch (NullPointerException e) {}
    	return hasSize;
    }
    
    /**
     * Determine if a map contains elements and is not null
     * @param val The map to evaluate
     * @return True if it is not null and has contains 1 or more elements, false otherwise
     */
    public static boolean hasSize(Map<?, ?> val) {
    	boolean hasSize = false;
    	try {
    		if ((val != null) && !val.isEmpty()){
    			hasSize = true;
    		}
    	} catch (NullPointerException e) {}
    	return hasSize;
    }
}
