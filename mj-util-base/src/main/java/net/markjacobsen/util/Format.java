/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Format {
	static Logger logger = LoggerProducer.getLogger(Format.class);
    
	public static String date(String format, Date date) {
		String ret = null;
		if (date != null) {
			DateFormat dateformat = new SimpleDateFormat(format);
			ret = dateformat.format(date);
		}
		return ret;
	}
	
	public static String date(String format, Calendar date) {
		return date(format, date.getTime());
	}
	
	public static String bigDecimal(BigDecimal n, int decimalPlaces) {
		return bigDecimal(n, decimalPlaces, true);
	}

	public static String bigDecimal(BigDecimal n, int decimalPlaces, boolean includeThousandsSeparator) {
		String format = null;

		if (!includeThousandsSeparator) {
			format = "#0." + repeatString("0", decimalPlaces - 1);
		} else {
			format = "#,##0." + repeatString("0", decimalPlaces - 1);
		}

		NumberFormat formatter = new DecimalFormat(format);
		return formatter.format(n);
	}
	
	/**
	 * Format a phone number. If the phone number is null or less than 2 characters, 
	 * just return the value passed in
	 * @param format
	 * @param phoneNumber
	 * @return
	 */
	public static String phoneNumber(FormatPhone format, String phoneNumber) {
		try {
			if (StringUtils.isNotEmpty(phoneNumber) && (phoneNumber.length() > 2)) {
				if (FormatPhone.TEN.equals(format)) {
					phoneNumber = stripNonNumeric(phoneNumber);
					if (phoneNumber.length() > 10) {
						phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
					}
				} else if (FormatPhone.DASH.equals(format)) {
					phoneNumber = stripNonNumeric(phoneNumber);
					if (phoneNumber.length() > 10) {
						phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
					}
					phoneNumber = phoneNumber.substring(0, 3) + "-" + phoneNumber.substring(3, 6) + "-" + phoneNumber.substring(6, 10);
				} else if (FormatPhone.DOT.equals(format)) {
					phoneNumber = stripNonNumeric(phoneNumber);
					if (phoneNumber.length() > 10) {
						phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
					}
					phoneNumber = phoneNumber.substring(0, 3) + "." + phoneNumber.substring(3, 6) + "." + phoneNumber.substring(6, 10);
				} else if (FormatPhone.INT.equals(format)) {
					phoneNumber = stripNonNumeric(phoneNumber);
					if (phoneNumber.length() == 10) {
						phoneNumber = "+1" + phoneNumber;
					} else if (phoneNumber.length() == 11) {
						phoneNumber = "+" + phoneNumber;
					}
				}
			}
		} catch(Exception e) {
			logger.warn("Problems formatting "+phoneNumber+". Returning as is.");
		}
		
		return phoneNumber;
	}
	
	public static String currency(int amount, boolean includeDecimals) {
		int decimalPlaces = 2;
		if (!includeDecimals) {
			decimalPlaces = 0;
		}
		return "$" + number(amount, decimalPlaces);
	}
	
	public static String currency(BigDecimal amount, boolean includeDecimals) {
		int decimalPlaces = 2;
		if (!includeDecimals) {
			decimalPlaces = 0;
		}
		return "$" + number(amount, decimalPlaces);
	}
	
	public static String number(double number, int decimalPlaces) {
		BigDecimal val = new BigDecimal(number);
		return number(val, decimalPlaces, true);
	}
	
	public static String number(int number, int decimalPlaces) {
		BigDecimal val = new BigDecimal(number);
		return number(val, decimalPlaces, true);
	}
	
	public static String number(BigDecimal number, int decimalPlaces) {
		return number(number, decimalPlaces, true);
	}

	public static String number(BigDecimal n, int decimalPlaces, boolean includeThousandsSeparator) {
		String format = null;
		String decimalFormat = "";
		
		if (decimalPlaces > 0) {
			decimalFormat = "." + repeatString("0", decimalPlaces);
		}
		
		if (!includeThousandsSeparator) {
			format = "#0" + decimalFormat;
		} else {
			format = "#,##0" + decimalFormat;
		}

		NumberFormat formatter = new DecimalFormat(format);
		return formatter.format(n);
	}

	public static String repeatString(String repeatThis, int repeatTimes) {
		StringBuffer buffer = new StringBuffer();
		for (int x = 0; x < repeatTimes; x++) {
			buffer.append(repeatThis);
		}
		return buffer.toString();
	}

	public static String upperCaseFirstChar(String value) {
		if (StringUtils.isEmpty(value)) {
			return value;
		}
		char[] titleCase = value.toCharArray();
		titleCase[0] = ("" + titleCase[0]).toUpperCase().charAt(0);
		return new String(titleCase);
	}

	public static String upperCaseFirstCharAllWords(String value) {
		if (StringUtils.isEmpty(value)) {
			return value;
		}
		StringBuffer sb = new StringBuffer();
		String[] words = value.split(" ");
		for (int x = 0; x < words.length; x++) {
			char[] titleCase = words[x].toLowerCase().toCharArray();
			if (titleCase.length >= 1) {
				titleCase[0] = ("" + titleCase[0]).toUpperCase().charAt(0);
				sb.append(new String(titleCase).trim());
				sb.append(" ");
			}
		}
		return sb.toString().trim();
	}

	public static String stripNonNumeric(String source) {
		String ret = source;
		if (StringUtils.isNotEmpty(source)) {
			ret = "";
			for (int x = 0; x < source.length(); x++) {
				if (Character.isDigit(source.charAt(x))) {
					ret += source.charAt(x);
				}
			}
		}
		return ret;
	}

	/**
	 * String numeric values. Note: Will trim returned result
	 * @param source
	 * @return
	 */
	public static String stripNumeric(String source) {
		String ret = source;
		if (StringUtils.isNotEmpty(source)) {
			ret = "";
			for (int x = 0; x < source.length(); x++) {
				if (!Character.isDigit(source.charAt(x))) {
					ret += source.charAt(x);
				}
			}
			ret = ret.trim();
		}
		return ret;
	}

	public static String stripCrLf(String source) {
		return replace(replace(source, "\n", ""), "\r", "");
	}
	
	/**
	 * String all HTML tags, and trim the result
	 * @param source
	 * @return
	 */
	public static String stripHtml(String source) {
		String ret = source;
		if (StringUtils.isNotEmpty(source)) {
			ret = source.replaceAll("\\<[^>]*>", "").trim();
		}
		return ret;
	}
	
	public static String stripExtraSpaces(String source) {
		String val = source;
		if (source != null) {
			val = source.replaceAll("\\s+", " ").trim();
		}
		return val;
	}
	
	public static String maxLenString(String val, int maxLen) {
		if ((val != null) && (val.length() > maxLen)) {
			val = val.substring(0, maxLen);
		}
		return val;
	}

	public static String replace(String source, String find, String replace) {
		return replace(source, find, replace, false);
	}

	public static String replace(String source, String find, String replace, boolean caseSensative) {
		if (source != null) {
			final int len = find.length();
			StringBuffer sb = new StringBuffer();
			int found = -1;
			int start = 0;

			if (caseSensative) {
				found = source.indexOf(find, start);
			} else {
				found = source.toLowerCase().indexOf(find.toLowerCase(), start);
			}

			while (found != -1) {
				sb.append(source.substring(start, found));
				sb.append(replace);
				start = found + len;

				if (caseSensative) {
					found = source.indexOf(find, start);
				} else {
					found = source.toLowerCase().indexOf(find.toLowerCase(), start);
				}
			}

			sb.append(source.substring(start));

			return sb.toString();
		} else {
			return "";
		}
	}

	public static String replaceSpan(String source, String findStart, String findEnd, String replace) {
		return replaceSpan(source, findStart, findEnd, replace, false);
	}

	/**
	 * Replace a span of text with the replace value. Useful for stripping html.
	 * 
	 * @param source The string to strip from
	 * @param findStart What you want to replace starts with
	 * @param findEnd What you want to replace ends with
	 * @param replace What to replace the span with
	 * @param caseSensative True if we want to perform a case sensative search
	 * @return String with all instances of the span stripped out
	 */
	public static String replaceSpan(String source, String findStart, String findEnd, String replace, boolean caseSensative) {
		if (source != null) {
			int findEndLen = findEnd.length();
			StringBuffer sb = new StringBuffer();
			int foundStart = -1;
			int foundEnd = -1;
			int start = 0;

			if (caseSensative) {
				foundStart = source.indexOf(findStart, start);
				foundEnd = source.indexOf(findEnd, start);
			} else {
				foundStart = source.toLowerCase().indexOf(findStart.toLowerCase(), start);
				foundEnd = source.toLowerCase().indexOf(findEnd.toLowerCase(), start);
			}

			while ((foundStart != -1) && (foundEnd != -1)) {
				sb.append(source.substring(start, foundStart));
				sb.append(replace);
				foundStart = foundEnd + findEndLen;
				start = foundStart;

				if (caseSensative) {
					foundStart = source.indexOf(findStart, start);
					foundEnd = source.indexOf(findEnd, start);
				} else {
					foundStart = source.toLowerCase().indexOf(findStart.toLowerCase(), start);
					foundEnd = source.toLowerCase().indexOf(findEnd.toLowerCase(), start);
				}
			}

			sb.append(source.substring(start));

			return sb.toString();
		} else {
			return "";
		}
	}
	
	public static String pad(String val, int totalChars) { return pad(val, totalChars, " "); }
	public static String pad(String val, int totalChars, String padChar) { return pad(val, totalChars, padChar, true); }
	public static String pad(String val, int totalChars, String padChar, boolean padRight) {
		int len = val.length();
		if (len < totalChars) {
			String pad = repeatString(padChar, totalChars - len);
			if (padRight) {
				val += pad;
			} else {
				val = pad + val;
			}
		}
		return val;
	}
}