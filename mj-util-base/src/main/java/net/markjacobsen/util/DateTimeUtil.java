/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class DateTimeUtil extends Format {
	@Inject
	Logger logger;
           
    @SuppressWarnings("deprecation")
	public Date time(int hour24, int minute) {
        Date date = new Date();
        date.setHours(hour24);
        date.setMinutes(minute);
       
        return date;
    }
    
    public static int millisecond(Calendar date) {
    	return date.get(Calendar.MILLISECOND);
    }
   
    public int second(Calendar date) {
    	return date.get(Calendar.SECOND);
    }
           
    public int second(Date date) {
    	return second(Convert.toCalendar(date));
    }
           
    public int minute(Calendar date) {
    	return date.get(Calendar.MINUTE);
    }
           
    public int minute(Date date) {
    	return minute(Convert.toCalendar(date));
    }
           
    public int hour24(Calendar date) {
    	return date.get(Calendar.HOUR_OF_DAY);
    }
           
    public int hour24(Date date) {
    	return hour24(Convert.toCalendar(date));
    }
   
    public int hour(Calendar date) {
    	return date.get(Calendar.HOUR);
    }
   
    public int hour(Date date) {
    	return hour(Convert.toCalendar(date));
    }
    
    public int hourMinAsInt(Date date) {
    	return Convert.toInt(formatDate("Hmm", date), false);
    }
   
    public int day(Calendar date) {
    	return date.get(Calendar.DAY_OF_MONTH);
    }
   
    public int day(Date date) {
    	return day(Convert.toCalendar(date));
    }
   
    public int dayOfWeek(Calendar date) {
    	return date.get(Calendar.DAY_OF_WEEK);
    }
           
    public int dayOfWeek(Date date) {
    	return dayOfWeek(Convert.toCalendar(date));
    }
    
    public int dayOfYear(Calendar date) {
    	return date.get(Calendar.DAY_OF_YEAR);
    }
    
    public int dayOfYear(Date date) {
    	return dayOfYear(Convert.toCalendar(date));
    }
           
    /**
	* Return the number of minutes for a given time
	* @param a_oDate Object containing the time
	* @return Minutes in the time
	*/
    public int minutes(Calendar date) {
        int min = date.get(Calendar.MINUTE);
        int hours = date.get(Calendar.HOUR_OF_DAY);
       
        return min + (hours * 60);
    }
   
    /**
	* Return the number of minutes for a given time
	* @param a_oDate Object containing the time
	* @return Minutes in the time
	*/
	public int minutes(Date date) {
        return minutes(Convert.toCalendar(date));
	}
           
	public Date minutesToTime(int min) throws Exception {
		String time;
        int hours = Convert.toInt( Math.floor(min / 60) );
        int minutes = min - (60 * hours);
           
        if (minutes < 10){
        	time = hours + ":0" + minutes;
        }else{
        	time = hours + ":" + minutes;
        }
               
        return Convert.toDate(time, FormatDate.TIME_24_HOUR.getFormat());
    }
   
    public Date[] minutesToTimeArray(int[] minutes) throws Exception {
        Date[] time = new Date[minutes.length];
        for (int x = 0; x < minutes.length; x++) {
            time[x] = minutesToTime(minutes[x]);
        }
        return time;
    }
   
    public int month(Calendar date) {
    	return date.get(Calendar.MONTH) + 1;
    }
   
    public int month(Date date) {
    	return month(Convert.toCalendar(date));
    }
    
    public int month(String date) {
    	if (date.length() >= 3) {
    		date = date.trim().substring(0, 3).toUpperCase();
	    	if (date.equals("JAN")) { return 1; }
	    	else if (date.equals("FEB")) { return 2; }
	    	else if (date.equals("MAR")) { return 3; }
	    	else if (date.equals("APR")) { return 4; }
	    	else if (date.equals("MAY")) { return 5; }
	    	else if (date.equals("JUN")) { return 6; }
	    	else if (date.equals("JUL")) { return 7; }
	    	else if (date.equals("AUG")) { return 8; }
	    	else if (date.equals("SEP")) { return 9; }
	    	else if (date.equals("OCT")) { return 10; }
	    	else if (date.equals("NOV")) { return 11; } 
	    	else if (date.equals("DEC")) { return 12; } 
	    	else { return -1;	}
    	} else {
    		try {
	    		int val = Convert.toInt(date, false);
	    		if ((val >= 1) && (val <= 12)) {
	    			return val;
	    		} else {
	    			return -1;
	    		}
    		} catch (Exception e) {
    			return -1;
    		}
    	}
    }
   
    public int month() {
        return month(new Date());
    }
   
    public int year(Calendar date) {
    	return date.get(Calendar.YEAR);
    }
   
    public int year(Date date) {
    	return year(Convert.toCalendar(date));
    }
   
    public int year() {
        return year(new Date());
    }
   
    /**
	* Add on to a date
	* @param a_oDate Date to add to
	* @param a_nInterval Number of units of datepart to add to date (positive, to get dates in the future; negative, to get dates in the past)
	* @param a_cDatePart s = seconds, n = minutes, h = hours, d = days, m = months, y = years
	* @return New date
	*/
    public Calendar dateAdd(Calendar date, int interval, DatePart datePart) {
    	Calendar cal = (Calendar)date.clone();
   
        switch (datePart) {
        	case SECOND:
        		cal.add(Calendar.SECOND, interval);
                break;
                                                             
            case MINUTE:
            	cal.add(Calendar.MINUTE, interval);
                break;
                                                             
            case HOUR:
            	cal.add(Calendar.HOUR, interval);
                break;
                                                             
            case DAY:
            	cal.add(Calendar.DATE, interval);
                break;
                           
            case MONTH:
            	cal.add(Calendar.MONTH, interval);
                break;
                           
            case YEAR:
            	cal.add(Calendar.YEAR, interval);
                break;
        }
   
        return cal;
    }
   
   
    public Date dateAdd(Date date, int interval, DatePart datePart) {
        return Convert.toDate(dateAdd(Convert.toCalendar(date), interval, datePart));
    }
   
   
    public int dateDiff(Date dayOne, Date dayTwo, DatePart datePart) {
    	int ret = 0;
               
        long milliseconds = dayTwo.getTime() - dayOne.getTime();
        if (milliseconds < 0) {
        	milliseconds = -milliseconds;
        }
        long seconds = milliseconds / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
                                       
        switch (datePart) {
        	case YEAR:
        		int yearOne = year(dayOne);
        		int yearTwo = year(dayTwo);
        		ret = yearTwo - yearOne;
        		break;
        
        	case MONTH:
        		int monthsOne = (year(dayOne) * 12) + month(dayOne);
        		int monthsTwo = (year(dayTwo) * 12) + month(dayTwo);
        		ret = monthsTwo - monthsOne;
                break;                         
                           
            case DAY:
            	ret = Convert.toInt(days);
                break;
                                       
            case HOUR:
            	ret = Convert.toInt(hours);
                break;
                                       
            case MINUTE:
            	ret = Convert.toInt(minutes);
                break;
                                       
            case SECOND:
            	ret = Convert.toInt(seconds);
                break;
        }
               
        return ret;
    }
   
    public int dateDiff(Calendar dayOne, Calendar dayTwo, DatePart datePart) {
        return dateDiff(Convert.toDate(dayOne), Convert.toDate(dayTwo), datePart);
    }
   
    public boolean datesEqual(Date date1, Date date2) {
    	return datesEqual(Convert.toCalendar(date1), Convert.toCalendar(date2));
    }
    
    public boolean datesEqual(Calendar date1, Calendar date2) {
    	return datesEqual(date1, date2, false);
    }
    
    public boolean datesEqual(Calendar date1, Calendar date2, boolean ignoreTime) {
    	boolean equal = false;
    	
    	if ((date1 != null) && (date2 != null)) {
    		if (ignoreTime == true) {
        		date1 = stripTime(date1);
        		date2 = stripTime(date2);
        	}
    		if (date1.compareTo(date2) == 0) {
    			equal = true;
    		}
    	}
    	
    	return equal;
    }
    
    @Deprecated
    public boolean dateEqual(Calendar date1, Calendar date2) {
    	if ((date1 != null) && (date2 != null) && (date1.compareTo(date2) == 0)) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    /**
     * Determine if a date is between (inclusive) 2 dates
     * @param dateToCheck
     * @param lowerDate
     * @param upperDate
     * @return
     */
    public boolean dateBetween(Calendar dateToCheck, Calendar lowerDate, Calendar upperDate) {
    	if (
    		(dateToCheck.after(lowerDate) && dateToCheck.before(upperDate)) ||
    		(dateEqual(dateToCheck, lowerDate)) ||
    		(dateEqual(dateToCheck, upperDate))) 
    	{
    		return true;
    	} else {
    		return false;
    	}
    }
   
    public String dayOfWeekAsString(Date date) {
    	return formatDate("EEEE", date);
    }
   
    public String monthAsString(Date date) {
    	return formatDate("MMMM", date);
    }
   
    public String monthAsString(int month) throws Exception {
        Date date = Convert.toDate(month + "/1/2000");
        return formatDate("MMMM", date);
    }
    
    public String timeFormat(Date date) {
    	return formatDate(FormatDate.TIME_12_HOUR.getFormat(), date);
    }
    
    public String dateFormat(Date date) {
        return formatDate(FormatDate.DEFAULT.getFormat(), date);
    }
    
    public String dateTimeFormat(Date date) {
        return formatDate(FormatDate.HUMAN.getFormat(), date);
    }
    
    public int dateAsEpoc(Date date) {
    	return Convert.toInt(date.getTime() / 1000);
    }
   
    public int weekInYear(Date date) {
        return weekInYear(date, Calendar.SUNDAY);
    }
   
    public int weekInYear(Date date, int firstDayOfWeek) {
        Calendar cal = Convert.toCalendar(date);
        cal.setFirstDayOfWeek(firstDayOfWeek);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }
    
    public Date combineDates(Date dateDate, Date timeDate) {
    	Calendar dateCal = Convert.toCalendar(dateDate);
    	dateCal.setTime(timeDate);
    	return Convert.toDate(dateCal);
    }
    
    public Calendar combineDates(Calendar dateDate, Calendar timeDate) {
    	setTime(dateDate, hour24(timeDate), minute(timeDate), second(timeDate), millisecond(timeDate));
    	return dateDate;
    }
    
    public Calendar stripTime(Calendar date) {
    	return setDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH)+1, date.get(Calendar.DAY_OF_MONTH));
    }
    
    /**
     * Sets a date, stripping out all time values
     * @param year
     * @param month
     * @param day
     * @return
     */
    public Calendar setDate(int year, int month, int day) {
    	return setDateTime(year, month, day, 0, 0, 0, 0);
    }
    
    public Calendar setTime(Calendar date, int hour24, int minute, int second, int millisecond) {
    	date.set(Calendar.HOUR_OF_DAY, hour24);
    	date.set(Calendar.MINUTE, minute);
    	date.set(Calendar.SECOND, second);
    	date.set(Calendar.MILLISECOND, millisecond);
    	return date;
    }
    
    public Calendar setDateTime(int year, int month, int day, int hour24, int minute, int second, int millisecond) {
    	Calendar date = Calendar.getInstance();
    	date.set(Calendar.MONTH, month-1);
    	date.set(Calendar.DAY_OF_MONTH, day);
    	date.set(Calendar.YEAR, year);
    	date.set(Calendar.HOUR_OF_DAY, hour24);
    	date.set(Calendar.MINUTE, minute);
    	date.set(Calendar.SECOND, second);
    	date.set(Calendar.MILLISECOND, millisecond);
    	return date;
    }
    
    /**
     * Convert a date in GMT to the local time
     * 
     * Hat tip to: http://stackoverflow.com/questions/10599109/how-to-convert-a-local-date-to-gmt
     * 
     * @param date Date with GMT value
     * @return Date in the local time
     */
    public Date gmtToLocal(Date gmtDate) {
    	try {
    		TimeZone localTimeZone = Calendar.getInstance().getTimeZone();
    		Date ret = new Date(gmtDate.getTime() - localTimeZone.getRawOffset());
    		
    		// If we are now in DST, back off by the delta.  
    		// Note that we are checking the GMT date, this is the KEY.
    		if (localTimeZone.inDaylightTime(gmtDate)) {
    			Date dstDate = new Date(ret.getTime() - localTimeZone.getDSTSavings());
    			
    			// Check to make sure we have not crossed back into standard time.
                // This happens when we are on the cusp of DST (7pm the day before the change for PDT)
    			if (localTimeZone.inDaylightTime(dstDate)) {
    				ret = dstDate;
    			}
    		}
    		
    		return ret;
        }
    	catch (Exception e) {e.printStackTrace(); return null; }
    }
    
    /**
     * Convert a date in GMT to the local time
     * 
     * Hat tip to: http://stackoverflow.com/questions/10599109/how-to-convert-a-local-date-to-gmt
     * 
     * @param date Date with GMT value
     * @return Date in the local time
     */
    public Calendar gmtToLocal(Calendar gmtCalendar) {
    	return Convert.toCalendar(gmtToLocal(Convert.toDate(gmtCalendar)));
    }
    
	private String formatDate(String format, Date date) {
		String ret = null;
		if (date != null) {
			DateFormat dateformat = new SimpleDateFormat(format);
			ret = dateformat.format(date);
		}
		return ret;
	}
    
    public Calendar standardizeDate(String val) {
    	// Order to try and standardize dates
        final String[] STD_DATE_FORMATS = new String[] {
        		FormatDate.DEFAULT.getFormat(),
        		"yyyy/MM/dd HH:mm:ss",
            	FormatDate.TIMESTAMP.getFormat(),
            	"MM-dd-yyyy HH:mm:ss",
            	"yyyy/MM/dd-HH:mm:ss",
            	"yyyy-MM-dd-HH:mm:ss",
            	"MM-dd-yyyy-HH:mm:ss",
            	"MM/dd/yyyy-HH:mm:ss",
                "yyyy/MM/dd",
            	FormatDate.FILE.getFormat(),
            	"MM-dd-yyyy",
            	FormatDate.TIMESTAMP.getFormat(),
            	"MMddyyyy"
            };
        
        Calendar result = null;
  	
    	// search through potential date formats to find a match
    	for(String potentialFormatMatch : STD_DATE_FORMATS) {
    		DateFormat source = new SimpleDateFormat(potentialFormatMatch);
    		source.setLenient(false);
    		try {
    			ParsePosition pos = new ParsePosition(0);
    			// no exception is being thrown by this parse method; instead null is returned
    			result = Convert.toCalendar(source.parse(val, pos));
    			
    			if (result == null) {
    				 throw new ParseException("Unparseable date: \"" + val + "\" for \"" + potentialFormatMatch + "\"" , pos.getErrorIndex());
    			} else if (pos.getIndex() != val.length()) {
    				result = null;
    			} 
    			
    			if (result != null) {
    				// Date parsed successfully so we're good
    				break;
    			}
    		} catch (ParseException ex) {
    			// probably just not a matching date format, continue to next possible match
    			continue;
    		}
    	}
    	
    	if (result == null) {
    		logger.warn("Unable to parse into date: "+val);
    	}
    	
    	return result;
    }
}