package net.markjacobsen.util.net.ssh;

import com.jcraft.jsch.UserInfo;

public class SSHUserInfo implements UserInfo {
	private String username;
	private String password;
	
	public String getPassphrase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean promptPassphrase(String arg0) {
		return false;
	}

	public boolean promptPassword(String arg0) {
		return false;
	}

	public boolean promptYesNo(String arg0) {
		return false;
	}

	public void showMessage(String arg0) {
		
	}
}
