package net.markjacobsen.util.net;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.inject.Inject;

import org.slf4j.Logger;

import net.markjacobsen.util.Format;
import net.markjacobsen.util.FormatDate;
import net.markjacobsen.util.net.ssh.SSHUserInfo;
import net.markjacobsen.util.net.ssh.SshFile;
import net.markjacobsen.util.system.FileUtil;
import net.markjacobsen.exceptions.InfrastructureException;
import net.markjacobsen.exceptions.ProcessingException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

/**
 * Use if you want to combine multiple operations over a single session. If you want one off actions, look at SshUtil.
 * @author MarkJacobsen.net
 */
public class SshSession {
	private SSHUserInfo userInfo;
	private boolean trustAllCerts = true;
	private boolean isSftp = false;
	private boolean verbose = false;
	private String keyfile;
	private String knownHostFile;
	private String host;
	private int port = 22;
	private Session session;
	private Channel channel;
	private ChannelSftp channelSftp;
	
	@Inject
	Logger logger;
	
	@Inject
	FileUtil fileUtil;
	
	public SshSession() {}
	
	public void connect(String host, String username, String password, boolean trustAllCerts) throws InfrastructureException {
		try {
			userInfo = new SSHUserInfo();
			userInfo.setUsername(username);
			userInfo.setPassword(password);
			setHost(host);
			setTrust(trustAllCerts);
			openSession();
		} catch (JSchException e) {
			throw new InfrastructureException(e);
		}
	}
	
	public void disconnect() {
		try { channelSftp.disconnect(); } catch (Exception e) {}
		try { channel.disconnect(); } catch (Exception e) {}
		try { session.disconnect(); } catch (Exception e) {}
		channelSftp = null;
		channel = null;
		session = null;
	}
	
	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public boolean getTrust() {
		return this.trustAllCerts;
	}
	
	public void setTrust(boolean trustAllCerts) {
		this.trustAllCerts = trustAllCerts;
	}
	
	public boolean getVerbose() {
		return this.verbose;
	}
	
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
	
	public String getKeyfile() {
		return this.keyfile;
	}
	
	public void setKeyfile(String keyfile) {
		this.keyfile = keyfile;
	}
	
	public String getKnownHostFile() {
		return this.knownHostFile;
	}
	
	public void setKnownHostFile(String knownHostFile) {
		this.knownHostFile = knownHostFile;
	}
	
	private Session openSession() throws JSchException, InfrastructureException {
		if (userInfo == null) {
			throw new InfrastructureException("SshUtil not initialized. Call init(user, pw, trust)");
		}
		
		if (session == null) {
			String username = userInfo.getUsername();
			JSch jsch = new JSch();
			if (null != getKeyfile()) {
				jsch.addIdentity(getKeyfile());
			}
	 
			if (!getTrust() && (getKnownHostFile() != null)) {
				logger.debug("Using known hosts: " + getKnownHostFile());
				jsch.setKnownHosts(getKnownHostFile());
			}
	 
			logger.debug("Getting session for {} on {}:{}", username, getHost(), getPort());
			session = jsch.getSession(username, getHost(), getPort());
			if (getTrust()) {
				logger.debug("Setting StrictHostKeyChecking=no");
				session.setConfig("StrictHostKeyChecking", "no");
			}
			session.setUserInfo(userInfo);
			session.setPassword(userInfo.getPassword());
		}
		
		if (!session.isConnected()) {
			logger.debug("Connecting to {} as {}", session.getHost(), session.getUserName());
			session.connect();
			logger.debug("Connected");
		}
		
		if (channel == null) {
			logger.trace("Opening channel");
			channel = session.openChannel("sftp");
			if (channel == null) {
				throw new InfrastructureException("channel is null");
			}
		}
		
		if (!channel.isConnected()) {
			logger.trace("Connecting to channel");
			channel.connect();
		}
		
		if (channelSftp == null) {
			logger.trace("Converting Channel to ChannelSftp");
			channelSftp = (ChannelSftp)channel;
		}
		
		return session;
	}
	
	/**
	 * Download a file from a unix host via sftp.
	 * @param host
	 * @param remoteFile
	 * @param localFile
	 * @throws ProcessingException
	 */
	public void downloadFile(String remoteFile, String localFile) throws ProcessingException {
		OutputStream output = null;
		
		try {
			if (remoteFile == null) {
				throw new InfrastructureException("remoteFile is null");
			}
			if (localFile == null) {
				throw new InfrastructureException("localFile is null");
			}			
			session = openSession();
			
			logger.trace("Getting OutputStream for {}", localFile);
			output = new FileOutputStream(localFile);
			logger.debug("Downloading {}: {} to {}", host, remoteFile, localFile);
			channelSftp.get(remoteFile, output);
			logger.debug("Download complete");
		} catch (JSchException | SftpException | FileNotFoundException | InfrastructureException e) {
			logger.error(e.getClass().getSimpleName()+": Unable to download "+remoteFile+" to "+localFile+" : "+e.getMessage());
			throw new ProcessingException(e);
		} finally {
			try { output.close(); } catch (Exception e) {}
		}
	}
	
	public List<SshFile> list(String path) throws ProcessingException {
		List<SshFile> files = new ArrayList<>();
		
		try {
			if (path == null) {
				throw new InfrastructureException("path is null");
			}			
			session = openSession();
			
			logger.info("Getting list on: {}", path);
			Vector list = channelSftp.ls(path);
			for (Object item : list) {
				ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry)item;
                String name = lsEntry.getFilename();
                SftpATTRS attrs = lsEntry.getAttrs();	
                int currentOldestTime = attrs.getMTime();
                String permissions = attrs.getPermissionsString();
                long size = attrs.getSize();
                Calendar modified = Calendar.getInstance();
                modified.setTimeInMillis(currentOldestTime * 1000L);
                boolean isDir = attrs.isDir();
                SshFile file = new SshFile(path, name, permissions, size, modified, isDir);
				files.add(file);
				logger.debug("{}/{} modified {}, perms: {}", file.getPath(), file.getFile(), Format.date(FormatDate.DB2_TIMESTAMP.getFormat(), file.getModified()), file.getPermissions());
			}
			logger.debug("List returned {} items", files.size());
		} catch (JSchException | SftpException | InfrastructureException e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage(), e);
			throw new ProcessingException(e);
		}
		
		return files;
	}
	
	/**
	 * Equivalent of ls command. Returns file names (just file/directory names, no path)
	 * @param path
	 * @return
	 * @throws ProcessingException
	 */
	public List<String> listFiles(String path) throws ProcessingException {
		List<String> files = new ArrayList<>();
		
		try {
			if (path == null) {
				throw new InfrastructureException("path is null");
			}
			List<SshFile> sshFiles = list(path);
			for (SshFile sshFile : sshFiles) {
				files.add(sshFile.getFile());
			}
		} catch (InfrastructureException e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage(), e);
			throw new ProcessingException(e);
		}
		
		return files;
	}
	
	public void download(String remotePath, String localDir) throws ProcessingException {
		try {
			if (remotePath == null) {
				throw new InfrastructureException("remotePath is null");
			}
			if (localDir == null) {
				throw new InfrastructureException("localDir is null");
			}
			session = openSession();
			
			if (remotePath.endsWith("/")) {
				remotePath = remotePath.substring(0, remotePath.length() - 1);
			}
			downloadRecurse(remotePath, localDir, channelSftp);
		} catch (InfrastructureException | JSchException e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage(), e);
			throw new ProcessingException(e);
		}
	}
	
	private void downloadRecurse(String remotePath, String localDir, ChannelSftp channelSftp) throws ProcessingException {
		try {
			logger.debug("Downloading contents of {} to {}", remotePath, localDir);
			if (fileUtil.folderExists(localDir) == false) {
				logger.debug("Creating dir: {}", localDir);
				fileUtil.createFolder(localDir);
			}
			Vector list = channelSftp.ls(remotePath);
			for (Object item : list) {
				ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry)item;
	            String name = lsEntry.getFilename();
	            if (!name.equals(".") && !name.equals("..")) {
		            SftpATTRS attrs = lsEntry.getAttrs();
		            if (attrs.isDir()) {
		            	String newRemotePath = remotePath+"/"+name;
		            	String newLocalDir = fileUtil.buildPath(localDir, name);
		            	downloadRecurse(newRemotePath, newLocalDir, channelSftp);
		            } else {
		            	String remoteFile = remotePath+"/"+name;
		            	String localFile = fileUtil.buildPath(localDir, name);
		            	try {
		            		downloadFile(remoteFile, localFile);
		            	} catch (ProcessingException e) {
		            		// keep going
		            	}
		            	/*
		            	OutputStream output = new FileOutputStream(localFile);
		    			logger.debug("Downloading {} to {}", remoteFile, localFile);
		    			try {
							channelSftp.get(remoteFile, output);
						} catch (SftpException e) {
							logger.error("Unable to download "+remoteFile+" to "+localFile+" : "+e.getMessage());
						} finally {
							output.close();
						}
						*/
		            }
	            }
			}
		} catch (SftpException e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage(), e);
			throw new ProcessingException(e);
		}		
	}
}
