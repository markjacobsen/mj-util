/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
package net.markjacobsen.util.net;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;

import org.slf4j.Logger;

import net.markjacobsen.util.Convert;
import net.markjacobsen.util.Validate;

@ApplicationScoped
public class EmailUtil {
	@Inject
	Logger logger;
	
	public static final String SMTP_SERVER = "127.0.0.1";
	public static final String SMTP_PORT = "25";
	public static final String SMTP_SERVER_GMAIL = "smtp.gmail.com";
	public static final String SMTP_PORT_GMAIL = "465";
	public static final String PROTOCOL_SMTP = "smtp";
	public static final String PROTOCOL_SMTPS = "smtps";
    
	public void sendGmail(String to, String from, String subject, String body, boolean htmlBody, String user, String pass) throws Exception {
		EmailMessage msg = new EmailMessage(to, from, subject, body);
		if (htmlBody) {
			msg.setBodyHtml(body);
		}
		sendGmail(msg, user, pass);
	}
	
	public void sendGmail(EmailMessage msg, String user, String pass) throws Exception {
		if (!Validate.hasLength(user) || !Validate.hasLength(pass)) {
			throw new Exception("You must supply values for the username and password");
		}
		Map<String, String> additionalProps = new HashMap<>();
		additionalProps.put("mail.smtp.socketFactory.port", EmailUtil.SMTP_PORT_GMAIL);
		additionalProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		sendEmail(msg, user, pass, SMTP_SERVER_GMAIL, PROTOCOL_SMTPS, SMTP_PORT_GMAIL, additionalProps);
	}
	
	public void sendEmail(String to, String from, String subject, String body, String smtpServer, String port) throws Exception {
		sendEmail(to, from, subject, body, false, null, null, smtpServer, null, port, null);
	}
	
	public void sendHtmlEmail(String to, String from, String subject, String body, String smtpServer, String port) throws Exception {
		sendEmail(to, from, subject, body, true, null, null, smtpServer, null, port, null);
	}
	
    public void sendEmail(String to, String from, String subject, String body, boolean htmlBody, String user, String pass, String smtpServer, String protocol, String port, Map<String, String> additionalProps) throws Exception {
    	EmailMessage msg = new EmailMessage(to, from, subject, body);
    	if (htmlBody) {
    		msg.setBodyHtml(body);
    	}
    	sendEmail(msg, user, pass, smtpServer, protocol, port, additionalProps);		
	}
    
    /**
     * Send an email message
     * @param msg The EmailMessage object containing details about the message to send
     * @param user SMTP username
     * @param pass SMTP password
     * @param smtpServer SMTP server
     * @param protocol SMTP protocol
     * @param port SMTP port
     * @throws Exception
     */
    public void sendEmail(EmailMessage msg, String user, String pass, String smtpServer, String protocol, String port, Map<String, String> additionalProps) throws Exception {	
    	boolean authenticatedSession = true;
    	if ((user == null) || (user.length() == 0)) {
    		authenticatedSession = false; 
    	}
		Properties sysProps = System.getProperties();
		sysProps.put("mail.smtp.host", smtpServer);
		if (protocol != null) {
			sysProps.put("mail.transport.protocol", protocol);
		}
		if (authenticatedSession) {
			sysProps.put("mail.smtps.auth", "true");
		}
		if (Validate.hasSize(additionalProps)) {
			for (String prop : additionalProps.keySet()) {
				sysProps.put(prop, additionalProps.get(prop));
			}
		}
		
		List<String> toArray = getRecipientArray(msg.getTo());
		List<String> ccArray = getRecipientArray(msg.getCc());
		List<String> bccArray = getRecipientArray(msg.getBcc());
		
		Session session = Session.getDefaultInstance(sysProps, null);
        
		MimeMessage message = new MimeMessage(session);
		
		if (Validate.hasLength(msg.getFromName())) {
			message.setFrom(new InternetAddress(msg.getFrom(), msg.getFromName()));
		} else {
			message.setFrom(new InternetAddress(msg.getFrom()));
		}
		
		for (String email : toArray) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		}
		
		if (Validate.hasSize(ccArray)) {
			for (String email : ccArray) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(email));
			}
		}
		
		if (Validate.hasSize(bccArray)) {
			for (String email : bccArray) {
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(email));
			}
		}
		
		message.setSubject(msg.getSubject());
		
		try {
			if ((msg.getAttachments() != null) && (msg.getAttachments().length > 0)) {
				// Adapeted From: http://www.codejava.net/java-ee/javamail/send-e-mail-with-attachment-in-java
				MimeMultipart multipart = new MimeMultipart();
				
		        // add text body part
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				if (Validate.hasLength(msg.getBodyHtml())) {
					messageBodyPart.setContent(msg.getBodyHtml(), "text/html; charset=utf-8");
				} else {
					messageBodyPart.setText(msg.getBody());
				}
		        multipart.addBodyPart(messageBodyPart);
				
		        // adds attachments
	            for (String[] attachment : msg.getAttachments()) {
	            	MimeBodyPart attachPart = new MimeBodyPart();
	            	attachPart.setContent(message, "multipart/mixed");
	            	
	                try  {
	                	String file = attachment[0];
	                	logger.trace("Adding attachment: {}", file);
	                	DataSource source = new FileDataSource(file);
	                    attachPart.setDataHandler(new DataHandler(source));
	                    
	                    if (attachment.length > 1) {
		                    String name = attachment[1];
		                    if (Validate.hasLength(name)) {
		                    	attachPart.setFileName(name);
		                    }
	                    }
	                    
	                    multipart.addBodyPart(attachPart);
	                } catch (Exception ex) {
	                    ex.printStackTrace();
	                }
	            }
		 
		        // sets the multi-part as e-mail's content
		        message.setContent(multipart);
			} else {
				if (Validate.hasLength(msg.getBodyHtml())){
					message.setContent(msg.getBodyHtml(), "text/html; charset=utf-8");
				} else {
					message.setText(msg.getBody());
				}
			}
		} catch (Exception e) {
			// this is what was working so use it as a fallback
			if ((msg.getBodyHtml() != null) && (msg.getBodyHtml().trim().length() > 0)){
				message.setText(msg.getBodyHtml(), "utf-8", "html");
			} else {
				message.setText(msg.getBody());
			}
		}
        
		logger.trace("Sending message to {}, cc {}, bcc {}, from {} w/ subject: {}", msg.getTo(), msg.getCc(), msg.getBcc(), msg.getFrom(), msg.getSubject());
		
		if (authenticatedSession){
			Transport transport = session.getTransport();
			transport.connect(smtpServer, Convert.toInt(port, false), user, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} else {
			Transport.send(message);
		}
    }
    
    private List<String> getRecipientArray(String recipients) {
    	List<String> returnArray = new ArrayList<>();
		if (Validate.hasLength(recipients)) {
			recipients = recipients.replace("\n", ";");
			recipients = recipients.replace(',', ';');
			recipients = recipients.replace(' ', ';');
			String[] tempArray = recipients.split(";");
			
			for (int x = 0; x < tempArray.length; x++) {
				String email = tempArray[x].trim();
				if (Validate.isEmail(email)) {
					returnArray.add(email);
				}
			}
		}
		return returnArray;
    }
    
    /**
     * Return all email addresses contained in a block of text
     * @param text
     * @return
     */
    public List<String> getEmailAddresses(String text) {
    	List<String> emails = new ArrayList<>();
    	
    	if (Validate.hasLength(text)) {
	    	Pattern p = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", Pattern.CASE_INSENSITIVE);
			Matcher matcher = p.matcher(text);
			while(matcher.find()) {
				emails.add(matcher.group());
			}
    	}
    		
    	return emails;
    }
}

