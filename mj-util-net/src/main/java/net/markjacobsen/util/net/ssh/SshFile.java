package net.markjacobsen.util.net.ssh;

import java.util.Calendar;

public class SshFile {
	private String path;
	private String file;
	private Calendar modified;
	private String permissions;
	private long size;
	private boolean isDir;
	
	public SshFile(String path, String file, String permissions, long size, Calendar modified, boolean isDir) {
		this.path = path;
		this.file = file;
		this.permissions = permissions;
		this.size = size;
		this.modified = modified;
		this.isDir = isDir;
	}
	
	public String getPath() {
		return path;
	}
	
	public String getFile() {
		return file;
	}
	
	public String getPermissions() {
		return permissions;
	}
	
	public long getSize() {
		return size;
	}
	
	public Calendar getModified() {
		return modified;
	}
	
	public boolean isDir() {
		return isDir;
	}
	
	public boolean isFile() {
		return !isDir;
	}
}
