package net.markjacobsen.util.net;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import net.markjacobsen.util.net.ssh.SshFile;
import net.markjacobsen.exceptions.InfrastructureException;
import net.markjacobsen.exceptions.ProcessingException;

/**
 * Use if you want one off actions. If you want to combine multiple actions, look at SshSession.
 * @author MarkJacobsen.net
 */
@ApplicationScoped
public class SshUtil {	
	@Inject
	private Logger logger;
	
	@Inject
	private SshSession session;
	
	public void downloadFile(String host, String username, String password, String remoteFile, String localFile) throws ProcessingException, InfrastructureException {
		logger.debug("Downloading {} to {}", remoteFile, localFile);
		session.connect(host, username, password, true);
		session.downloadFile(remoteFile, localFile);
		session.disconnect();
		logger.debug("Done");
	}
	
	public void download(String host, String username, String password, String remotePath, String localDir) throws InfrastructureException, ProcessingException {
		logger.debug("Downloading {} to {}", remotePath, localDir);
		session.connect(host, username, password, true);
		session.download(remotePath, localDir);
		session.disconnect();
		logger.debug("Done");
	}
	
	public List<String> listFiles(String host, String username, String password, String path) throws ProcessingException, InfrastructureException {
		logger.debug("Listing {}", path);
		session.connect(host, username, password, true);
		List<String> files = session.listFiles(path);
		session.disconnect();
		logger.debug("Done");
		return files;
	}
	
	public List<SshFile> list(String host, String username, String password, String path) throws ProcessingException, InfrastructureException {
		logger.debug("Listing {}", path);
		session.connect(host, username, password, true);
		List<SshFile> files = session.list(path);
		session.disconnect();
		logger.debug("Done");
		return files;
	}
}
