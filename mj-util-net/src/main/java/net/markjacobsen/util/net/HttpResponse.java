package net.markjacobsen.util.net;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class HttpResponse {
	private int code = 0;
	private String message = null;
	private String content = null;
	private org.apache.http.HttpResponse originalResponse = null;
	
	public HttpResponse() {}

	/**
	 * Check if response code is a success (2xx)
	 * @return True if response code between 200-299, else false
	 */
	public boolean isSuccess() {
		if ((getCode() >= 200) && (getCode() <= 299)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if response code is a redirect (3xx)
	 * @return True if response code between 300-399, else false
	 */
	public boolean isRedirect() {
		if ((getCode() >= 300) && (getCode() <= 399)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if response code is an error of any sort (client or server)
	 * @return True if response code between 400-599, else false
	 */
	public boolean isError() {
		if ((getCode() >= 400) && (getCode() <= 599)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if response code is a client error (4xx)
	 * @return True if response code between 400-499, else false
	 */
	public boolean isClientError() {
		if ((getCode() >= 400) && (getCode() <= 499)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if response code is a server error (5xx)
	 * @return True if response code between 500-599, else false
	 */
	public boolean isServerError() {
		if ((getCode() >= 500) && (getCode() <= 599)) {
			return true;
		} else {
			return false;
		}
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public org.apache.http.HttpResponse getOriginalResponse() {
		return originalResponse;
	}
	
	public void setOriginalResponse(org.apache.http.HttpResponse originalResponse) {
		this.originalResponse = originalResponse;
	}
}
