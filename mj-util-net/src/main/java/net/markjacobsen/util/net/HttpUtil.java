package net.markjacobsen.util.net;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;


import net.markjacobsen.domain.KeyValueContainer;
import net.markjacobsen.exceptions.NetworkException;
import net.markjacobsen.exceptions.ValidationException;
import net.markjacobsen.util.Convert;
import net.markjacobsen.util.Validate;
import net.markjacobsen.util.system.SystemUtil;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 * 
 * Ref: http://www.baeldung.com/httpclient-post-http-request
 */
@ApplicationScoped
public class HttpUtil {
	@Inject
	Logger logger;
	
	@Inject
	SystemUtil sysUtil;
		
	/**
	 * Given a HttpServletRequest, get the full URL
	 * @param request
	 * @param includeQueryString
	 * @return
	 */
	public String getUrl(HttpServletRequest request, boolean includeQueryString) {
		StringBuffer url = request.getRequestURL();
		if (request.isSecure() == true) {
			url = new StringBuffer(url.toString().replace("http:", "https:"));
		}
		if ((includeQueryString == true) && (request.getQueryString() != null)) {
			url.append("?");
			url.append(request.getQueryString());
		}
		return url.toString();
	}
	
	/**
	 * Given a URL and queryParams, return a full url w/ the params (encoded) added.
	 * If no params, return just the url 
	 * @param url
	 * @param queryParams 
	 * @return
	 */
	public String buildUrl(String url, Map<String, String> queryParams) {
		try {
			if (Validate.hasSize(queryParams)) {
				String queryStr = encodeParams(queryParams);
				url += "?" + queryStr;
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Error building url for "+url, e);
			url = null;
		}
		return url;
	}
	
	/**
	 * Download a file from a URL to the local machine
	 * @param url
	 * @param localFile
	 * @throws NetworkException
	 */
	public void downloadFile(String url, String localFile) throws NetworkException {
		URL urlObject = null;
		HttpURLConnection connection = null;
		InputStream stream = null;
		OutputStream out = null;
		
		try {
			urlObject = new URL(url);
			connection = (HttpURLConnection)urlObject.openConnection();
			stream = connection.getInputStream();
	        out = new FileOutputStream(localFile);
	        byte buf[]= new byte[4096];
	        int read = -1;
	        logger.trace("Start downloading {} to {}", url, localFile);
	        while ((read = stream.read(buf)) != -1) {
	        	if (read > 0) {
	        		out.write(buf, 0, read);
	        	}
	        }
	        logger.trace("Finished downloading {} to {}", url, localFile);
		} catch (IOException e) {
			throw new NetworkException("Error during downloadFile: " + e.getMessage(), e);
		} finally {
			try{ out.close(); }	catch (Exception e) {}
			try{ stream.close(); }	catch (Exception e) {}
			try{ connection.disconnect(); }	catch (Exception e) {}
		}
	}
	
	/**
	 * Given a map of parameters, return a URL encoded query string w/ all the entries
	 * @param params
	 * @return Query string or zero length string if no params
	 * @throws UnsupportedEncodingException
	 */
	public String encodeParams(Map<String, String> params) throws UnsupportedEncodingException {
		String ret = "";
		if (Validate.hasSize(params)) {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				ret += entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8") + "&";
			}
			if ((ret.length() > 0) && (ret.charAt(ret.length() - 1) == '&')) {
				// Strip the last ampersand
				ret = ret.substring(0, ret.length() - 1);
			}
		}
		return ret;
	}
	
	public void setupProxy() {		
		if (System.getProperties().get("http.proxyHost") != null) {
			logger.debug("Proxy already setup via system properties");
		} else {
			String proxy = null;
			String envVar = null;
			if ((envVar == null) && (sysUtil.getEnvVal("HTTP_PROXY") != null)) { envVar = "HTTP_PROXY"; }
			if ((envVar == null) && (sysUtil.getEnvVal("HTTPS_PROXY") != null)) { envVar = "HTTPS_PROXY"; }
			if ((envVar == null) && (sysUtil.getEnvVal("http_proxy") != null)) { envVar = "http_proxy"; }
			if ((envVar == null) && (sysUtil.getEnvVal("https_proxy") != null)) { envVar = "https_proxy"; }
			
			if (envVar != null) {
				logger.debug("Setting up proxy from env var = {}", envVar);
				proxy = sysUtil.getEnvVal(envVar);
				String[] parts = proxy.split("@");
				if (parts.length == 1) {
					String[] serverParts = parts[0].split(":");
					
					System.getProperties().put("http.proxyHost", serverParts[0]);
					System.getProperties().put("https.proxyHost", serverParts[0]);
					
					if (serverParts.length > 1) {
						System.getProperties().put("http.proxyPort", serverParts[1]);
						System.getProperties().put("https.proxyPort", serverParts[1]);
					}
				} else if (parts.length == 2) {
					String[] userParts = parts[0].replace("http://", "").split(":");
					String[] serverParts = parts[1].split(":");
					
					System.getProperties().put("http.proxyUser", userParts[0]);
					System.getProperties().put("https.proxyUser", userParts[0]);
					
					if (userParts.length > 1) {
						System.getProperties().put("http.proxyPassword", userParts[1]);
						System.getProperties().put("https.proxyPassword", userParts[1]);
					}
					
					System.getProperties().put("http.proxyHost", serverParts[0]);
					System.getProperties().put("https.proxyHost", serverParts[0]);
					
					if (serverParts.length > 1) {
						System.getProperties().put("http.proxyPort", serverParts[1]);
						System.getProperties().put("https.proxyPort", serverParts[1]);
					}
				}
				
				logger.debug("http.proxyHost = {}", System.getProperties().get("http.proxyHost"));
				logger.debug("http.proxyPort = {}", System.getProperties().get("http.proxyPort"));
				logger.debug("http.proxyUser = {}", System.getProperties().get("http.proxyUser"));
				logger.trace("http.proxyPassword = {}", System.getProperties().get("http.proxyPassword"));
				
				logger.debug("https.proxyHost = {}", System.getProperties().get("https.proxyHost"));
				logger.debug("https.proxyPort = {}", System.getProperties().get("https.proxyPort"));
				logger.debug("https.proxyUser = {}", System.getProperties().get("https.proxyUser"));
				logger.trace("https.proxyPassword = {}", System.getProperties().get("https.proxyPassword"));
			}
		}
	}
	
	public HttpResponse httpPost(String url, 
								Map<String, String> queryParams, 
								Map<String, String> reqProps, 
								Map<String, String> headers,
								String jsonBody,
								File fileToUpload,
								boolean setupProxy) throws NetworkException {
		HttpResponse resp = new HttpResponse();
		try {
			url = buildUrl(url, queryParams);
			
			if (setupProxy) { 
				setupProxy();
			}
			
			logger.trace("Posting to: {}", url);
			CloseableHttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(url);
			
			if (Validate.hasSize(headers)) {
				for (String key : headers.keySet()) {
					httpPost.setHeader(key, headers.get(key));
				}
			}
						
			if (Validate.hasSize(reqProps)) {
				List<NameValuePair> params = new ArrayList<>();
				for (String key : reqProps.keySet()) {
				    params.add(new BasicNameValuePair(key, reqProps.get(key)));
				}
			    httpPost.setEntity(new UrlEncodedFormEntity(params));
			}
			
			if (Validate.hasLength(jsonBody)) {
				logger.trace("Setting JSON body: {}", jsonBody);
				StringEntity entity = new StringEntity(jsonBody);
			    httpPost.setEntity(entity);
			    httpPost.setHeader("Accept", "application/json");
			    httpPost.setHeader("Content-type", "application/json");
			}
			
			if (fileToUpload != null) {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			    builder.addBinaryBody("file", fileToUpload, ContentType.APPLICATION_OCTET_STREAM, fileToUpload.getName());
			    HttpEntity multipart = builder.build();
			    httpPost.setEntity(multipart);
			}
			
			CloseableHttpResponse response = client.execute(httpPost);
			
			logger.debug("Processing POST response");
			ResponseHandler<String> handler = new BasicResponseHandler();
			resp.setContent(handler.handleResponse(response));
			resp.setCode(response.getStatusLine().getStatusCode());
			resp.setMessage(response.getStatusLine().getReasonPhrase());
			resp.setOriginalResponse(response);
			
			return resp;
		} catch (IOException e) {
			throw new NetworkException("Error during httpPost: " + e.getMessage(), e);
		}
	}
	
	public HttpResponse httpGet(String url, 
								Map<String, String> queryParams, 
								Map<String, String> headers,
								boolean setupProxy) throws NetworkException {
		HttpResponse resp = new HttpResponse();
		try {
			url = buildUrl(url, queryParams);
			
			if (setupProxy) { 
				setupProxy();
			}
			
			logger.trace("Getting: {}", url);
			CloseableHttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			
			if (Validate.hasSize(headers)) {
				for (String key : headers.keySet()) {
					httpGet.setHeader(key, headers.get(key));
				}
			}
			
			CloseableHttpResponse response = client.execute(httpGet);
			
			logger.debug("Processing GET response");
			ResponseHandler<String> handler = new BasicResponseHandler();
			resp.setContent(handler.handleResponse(response));
			resp.setCode(response.getStatusLine().getStatusCode());
			resp.setMessage(response.getStatusLine().getReasonPhrase());
			resp.setOriginalResponse(response);
			
			return resp;
		} catch (IOException e) {
			throw new NetworkException("Error during httpGet: " + e.getMessage(), e);
		}
	}
	
	public HttpResponse httpPut(String url, 
								Map<String, String> queryParams, 
								Map<String, String> reqProps, 
								Map<String, String> headers,
								String jsonBody,
								File fileToUpload,
								boolean setupProxy) throws NetworkException {
		HttpResponse resp = new HttpResponse();
		try {
			url = buildUrl(url, queryParams);
			
			if (setupProxy) { 
				setupProxy();
			}
			
			logger.trace("Putting to: {}", url);
			CloseableHttpClient client = HttpClientBuilder.create().build();
			HttpPut httpPut = new HttpPut(url);
			
			if (Validate.hasSize(headers)) {
				for (String key : headers.keySet()) {
					httpPut.setHeader(key, headers.get(key));
				}
			}
						
			if (Validate.hasSize(reqProps)) {
				List<NameValuePair> params = new ArrayList<>();
				for (String key : reqProps.keySet()) {
				    params.add(new BasicNameValuePair(key, reqProps.get(key)));
				}
				httpPut.setEntity(new UrlEncodedFormEntity(params));
			}
			
			if (Validate.hasLength(jsonBody)) {
				logger.trace("Setting JSON body: {}", jsonBody);
				StringEntity entity = new StringEntity(jsonBody);
				httpPut.setEntity(entity);
				httpPut.setHeader("Accept", "application/json");
				httpPut.setHeader("Content-type", "application/json");
			}
			
			if (fileToUpload != null) {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			    builder.addBinaryBody("file", fileToUpload, ContentType.APPLICATION_OCTET_STREAM, fileToUpload.getName());
			    HttpEntity multipart = builder.build();
			    httpPut.setEntity(multipart);
			}
			
			CloseableHttpResponse response = client.execute(httpPut);
			
			logger.debug("Processing PUT response");
			ResponseHandler<String> handler = new BasicResponseHandler();
			resp.setContent(handler.handleResponse(response));
			resp.setCode(response.getStatusLine().getStatusCode());
			resp.setMessage(response.getStatusLine().getReasonPhrase());
			resp.setOriginalResponse(response);
			
			return resp;
		} catch (IOException e) {
			throw new NetworkException("Error during httpPut: " + e.getMessage(), e);
		}
	}
	
	public int getParamAsInt(HttpServletRequest request, String param) throws ValidationException {
		String paramVal = null;
		try {
			paramVal = request.getParameter(param);
			if (paramVal != null) {
				return Convert.toInt(paramVal, false);
			} else {
				throw new ValidationException("No parameter named: " + param);
			}
		} catch (Exception e) {
			throw new ValidationException("Unable to get int value for " + param + " = " + paramVal, e);
		}
	}
	
	/**
	 * Given a URL, return the protocol (ex: http)
	 * @param url
	 * @return
	 */
	public String getProtocol(String url) {
		return url.split(":")[0].toLowerCase();
	}
	
	/**
	 * Given a URL, return the domain name
	 * @param url
	 * @return
	 */
	public String getDomain(String url) {
		String protocol = getProtocol(url);
		url = url.substring(protocol.length() + 3); // + 3 to get rid of ://
		return url.split("\\/")[0];
	}
	
	/**
	 * Given a URL, return only the script (ex: /some/page.txt)
	 * @param url
	 * @return
	 */
	public String getScript(String url) {
		String protocol = getProtocol(url);
		String domain = getDomain(url);
		String qs = getQueryString(url);
		
		url = url.replace(protocol + "://" + domain, "");
		
		if (qs.length() > 0) {
			url = url.replace("?" + qs, "");
		} else if (url.substring(url.length() - 1).equalsIgnoreCase("?") == true) {
			url = url.substring(0, url.length() - 1);
		}
		
		return url;
	}
	
	/**
	 * Given a URL, return the query string (i.e. everything after the ?)
	 * @param url URL to extract query string from
	 * @return Query String, or zero length string if none
	 */
	public String getQueryString(String url) {
		int start = url.indexOf("?");
		if (start >= 0) {
			return url.substring(start+1);
		} else {
			return "";
		}
	}
	
	/**
	 * Given a URL, strip out the query string (assuming the value passed is a query string
	 * if one is not pulled out), and return a List of the key value pairs in KeyValueContainer objects
	 * @param url
	 * @return
	 */
	public List<KeyValueContainer> getQueryStringValues(String url) {
		List<KeyValueContainer> vals = new ArrayList<KeyValueContainer>();
		String queryStr = getQueryString(url);
		if (queryStr.length() == 0) { queryStr = url; }
		
		String[] pairs = queryStr.split("&");
		for (int x = 0; x < pairs.length; x++) {
			if (pairs[x].length() > 0) {
				KeyValueContainer val = null;
				String[] pair = pairs[x].split("=");
				String key = pair[0].trim();
				if (pair.length == 1) {
					val = new KeyValueContainer(key, null);
				} else if (pair.length == 2) {
					val = new KeyValueContainer(key, pair[1].trim());
				} else if (pair.length > 2) {
					String tmp = "";
					for (int y = 1; y < pair.length; y++) {
						tmp += pair[y] + "=";
					}
					val = new KeyValueContainer(key, tmp.substring(0, tmp.length() - 1).trim());
				}
				vals.add(val);
			}
		}
		
		return vals;
	}
	
	public String getMailtoLink(String to, String linkText) { return getMailtoLink(to, linkText, null); }
	public String getMailtoLink(String to, String linkText, String subject) { return getMailtoLink(to, linkText, subject, ""); }
	public String getMailtoLink(String to, String linkText, String subject, String body) { return getMailtoLink(to, linkText, subject, body, null); }
	public String getMailtoLink(String to, String linkText, String subject, String body, String cc) { return getMailtoLink(to, linkText, subject, body, cc, null); }
	public String getMailtoLink(String to, String linkText, String subject, String body, String cc, String bcc) {
		try {
			String qs = "";
			if ((cc != null) && (cc.trim().length() > 0)){ qs += "cc="+cc+"&"; }
			if ((bcc != null) && (bcc.trim().length() > 0)){ qs += "bcc="+bcc+"&"; }
			if ((subject != null) && (subject.trim().length() > 0)){ qs += "subject="+subject.replace("&", "%26")+"&"; }
			if ((body != null) && (body.trim().length() > 0)){ qs += "body="+body.replace("&", "%26")+"&"; }
			
			return "<a href=\"mailto:"+to+"&"+qs+"\">"+linkText+"</a>";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getMailtoLink(String to, String linkText, String subject, String[] body) {
		String newBody = "";
		for (int x = 0; x < body.length; x++) {
			newBody += body[x] + "%0A";
		}
		return getMailtoLink(to, linkText, subject, newBody);
	}
	
	public String getMailtoLink(EmailMessage email, String linkText) {
		return getMailtoLink(email.getTo(), linkText, email.getSubject(), email.getBody(), email.getCc(), email.getBcc());
	}
	
	public void writeServletTextResponse(HttpServletResponse response, String body) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		response.getWriter().write(body);
	}
}
