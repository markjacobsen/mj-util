package net.markjacobsen.util.net;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(WeldJUnit4Runner.class)
public class EmailUtilsTest {
	@Inject
	EmailUtil emailUtil;
	
	@Test
	public void getEmailsTest() {
		String text = "John Doe <jdoe@example.com>,steve@there.com";
		List<String> emails = emailUtil.getEmailAddresses(text);
		assertEquals(emails.size(), 2);
		assertEquals(emails.get(0), "jdoe@example.com");
		assertEquals(emails.get(1), "steve@there.com");
		
		text = "John Doe <jdoe@example.com>, \n;steve@there.com";
		emails = emailUtil.getEmailAddresses(text);
		assertEquals(emails.size(), 2);
		assertEquals(emails.get(0), "jdoe@example.com");
		assertEquals(emails.get(1), "steve@there.com");
		
		text = null;
		emails = emailUtil.getEmailAddresses(text);
		assertEquals(emails.size(), 0);
	}
}
