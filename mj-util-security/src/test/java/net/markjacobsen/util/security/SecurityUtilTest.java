package net.markjacobsen.util.security;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.markjacobsen.exceptions.InfrastructureException;

@RunWith(WeldJUnit4Runner.class)
public class SecurityUtilTest {
	@Inject
	SecurityUtil secUtil;
	
	@Test
	public void testEncryptDecrypt() {
		String privateKey = "IlikeChee$e";
		String pw = "MyTestValue$*!";
		String encrypted;
		try {
			encrypted = secUtil.encrypt(pw, privateKey);
			assertEquals(pw, secUtil.decrypt(encrypted, privateKey));
		} catch (InfrastructureException e) {
			e.printStackTrace();
		}
	}
}
