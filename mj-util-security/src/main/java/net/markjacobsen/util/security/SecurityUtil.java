package net.markjacobsen.util.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;

import net.markjacobsen.exceptions.InfrastructureException;
import net.markjacobsen.util.Convert;
import net.markjacobsen.util.MiscUtil;
import net.markjacobsen.util.Validate;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
@ApplicationScoped
public class SecurityUtil {
	private static final String CHARSET = "UTF8";
	
	private String algorithm = "DES";
	private SecretKey key = null;
	private String secretKey = null;
	private DESKeySpec keySpec = null;
	private SecretKeyFactory keyFactory = null;
	
	@Inject
    Logger logger;
	
	@Inject
	MiscUtil miscUtil;
    
    /**
     * Convenience method for prompting for a password
     * @param prompt What to prompt the user with (ex: "Password:")
     * @return The value the user types in
     */
    public String promptPassword(String prompt) {
    	return miscUtil.prompt(prompt, null, true);
    }
    
    public String promptPassword() {
    	return promptPassword("Password");
    }
	
	public String encrypt(String value, String privateKey) throws InfrastructureException {
		try {
			if (!Validate.hasLength(value)) {
				logger.warn("No value passed in. Returning zero length string.");
				return "";
			} else {
				logger.trace("Encrypting \"{}\"", value, this.key);
				initCipher(privateKey);
				byte[] cleartext = value.getBytes(CHARSET);
				Cipher cipher = Cipher.getInstance(this.algorithm);
				cipher.init(Cipher.ENCRYPT_MODE, this.key);
				String encryptedValue = Convert.toString(Base64.encodeBase64(cipher.doFinal(cleartext)));
				logger.trace("Encrypted value \"{}\"", encryptedValue);
				return encryptedValue;
			}
		}
		catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | 
				IllegalBlockSizeException | UnsupportedEncodingException e)
		{
			throw new InfrastructureException("Error attempting to encrypt value", e);
		}
	}
	
	public String decrypt(String value, String privateKey) throws InfrastructureException {
		try {
			if (!Validate.hasLength(value)) {
				logger.warn("No value passed in. Returning zero length string.");
				return "";
			} else {
				logger.trace("Decrypting \"{}\"", value, this.key);
				initCipher(privateKey);
				byte[] encrypedPwdBytes = Base64.decodeBase64(value.getBytes());
				Cipher cipher = Cipher.getInstance(this.algorithm);
				cipher.init(Cipher.DECRYPT_MODE, this.key);
				byte[] plainTextPwdBytes = (cipher.doFinal(encrypedPwdBytes));
				String decryptedValue = Convert.toString(plainTextPwdBytes);
				logger.trace("Decrypted value \"{}\"", decryptedValue);
				return decryptedValue;
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException |
				IllegalBlockSizeException e) {
			throw new InfrastructureException("Error attempting to decrypt value", e);
		}
	}
	
	public void changeAlgorithm(String algorithm) {
		logger.info("Changing algorithm to {}", algorithm);
		this.algorithm = algorithm;
	}
	
	private void initCipher(String privateKey) throws InfrastructureException {
		String secretKey = privateKey;
		try {
			if (secretKey.length() < 16) {
				logger.trace("Padding secretKey");
				secretKey += "----------------";
				secretKey = secretKey.substring(0, 16);
			}
			this.secretKey = secretKey;
			this.keySpec = new DESKeySpec(this.secretKey.getBytes(CHARSET));
			this.keyFactory = SecretKeyFactory.getInstance(this.algorithm);
			this.key = this.keyFactory.generateSecret(this.keySpec);
		} catch (InvalidKeyException | InvalidKeySpecException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new InfrastructureException("Error initializing cipher", e);
		}
	}
}
