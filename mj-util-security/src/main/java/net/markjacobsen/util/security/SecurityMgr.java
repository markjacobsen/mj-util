package net.markjacobsen.util.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;

import net.markjacobsen.exceptions.FileSystemException;
import net.markjacobsen.exceptions.InfrastructureException;
import net.markjacobsen.util.Convert;
import net.markjacobsen.util.Validate;
import net.markjacobsen.util.system.FileUtil;
import net.markjacobsen.util.system.WorkUtil;

/**
 * @author markjacobsen.net 
 * Visit: http://markjacobsen.net/code
 * 
 * Must keep full class header.
 * Free to use, modify, and redistribute.
 */
public class SecurityMgr {
	@Inject
	Logger logger;
	
	@Inject
	FileUtil fileUtil;
	
	@Inject
	WorkUtil workUtil;
	
	public static final String PROP_FILE = "security.properties";
	public static final boolean CREATE_FILE = false;
	
	private Map<String, Entry> entries = new Hashtable<String, Entry>();
	private String file = null;
	private SecurityCipher cipher = null;
	
	public SecurityMgr(String masterKey) throws FileSystemException, InfrastructureException {
		this(masterKey, null);
	}
	
	public SecurityMgr(String masterKey, String file) throws FileSystemException, InfrastructureException {
		this(masterKey, file, SecurityMgr.CREATE_FILE);
	}
	
	public SecurityMgr(String masterKey, String file, boolean createPropFileIfNew) throws FileSystemException, InfrastructureException {		
		this.loadFile(masterKey, file, createPropFileIfNew);
	}
		
	public void loadFile(String masterKey, String file) throws FileSystemException, InfrastructureException { this.loadFile(masterKey, file, SecurityMgr.CREATE_FILE); }
	@SuppressWarnings("resource")
	public void loadFile(String masterKey, String file, boolean createPropFileIfNew) throws FileSystemException, InfrastructureException {
		InputStream inputStream = null;
		Properties props = new Properties();
		
		try {
			if (file == null) {
				file = workUtil.getDirConfig() + fileUtil.getPathSeparator() + PROP_FILE;
			}
			
			this.cipher = new SecurityCipher(masterKey);
			this.file = file;
		
			if ((this.file != null) && (!fileUtil.fileExists(this.file)) && (createPropFileIfNew)) {
				logger.debug("Attempting to create file: {}", this.file);
				this.save();
			}
			
			if (fileUtil.fileExists(this.file)) {
				logger.info("Loading from passed in file: {}", this.file);
				inputStream = new FileInputStream(this.file);
			} else {
				logger.info("Attempting to find file on classpath: {}", SecurityMgr.PROP_FILE);
				inputStream = this.getClass().getClassLoader().getResourceAsStream(SecurityMgr.PROP_FILE);
			}
			
			if (inputStream == null) {
				throw new InfrastructureException("Invalid password file or no default file \""+SecurityMgr.PROP_FILE+"\" found on the classpath");
			} else {
				logger.debug("Loading password file");
				
				props.load(inputStream);
				inputStream.close();
				
				if (props.getProperty("keys") == null) {
					logger.warn("No \"keys\" property exists so nothing will be read");
				} else {
					String[] keys = props.getProperty("keys").split(",");
					
					for (String key : keys) {
						logger.trace(key);
						String user = props.getProperty(key + ".username");
						String password = props.getProperty(key + ".password");
						String note = props.getProperty(key + ".note");
						
						if (Validate.hasLength(user)) { user = this.cipher.decrypt(user); }
						if (Validate.hasLength(password)) { password = this.cipher.decrypt(password); }
						
						Entry entry = new Entry(user, password, note);
		
						this.entries.put(key, entry);
					}
				}
			}
			
			logger.debug("Loaded {} entries", this.entries.size());
		} catch (FileNotFoundException e) {
			throw new FileSystemException("FileNotFound", e);
		} catch (IOException e) {
			throw new FileSystemException("IOException", e);
		}
	}
	
	private boolean save() throws FileSystemException {
		if (this.file == null) {
			logger.warn("No file to save to");
			return false;
		} else {
			List<String> lines = new ArrayList<String>();
			logger.debug("Saving to file {}", this.getFile());
			
			lines.add("#--------------------------------------------------------------------------------------");
			lines.add("# Usernames and Passwords do need to be encrypted using the SecurityCipher class.");
			lines.add("# It is suggested that you use the PasswordManager app in cffreedom-cl-apps to maintain");
			lines.add("# this file.");
			lines.add("#--------------------------------------------------------------------------------------");
			lines.add("");
			
			if (this.entries.size() <= 0)
			{
				logger.warn("No Entry objects cached so no actual values will be written");
				lines.add("# No entries to save");
			}
			else
			{
				lines.add("keys=" + Convert.toDelimitedString(this.entries.keySet(), ","));
				lines.add("");
				
				for (String key : this.entries.keySet())
				{
					logger.trace(key);
					Entry entry = this.getEntry(key);
					lines.add(key + ".username=" + this.getPropFileValue(entry.username, true));
					lines.add(key + ".password=" + this.getPropFileValue(entry.password, true));
					lines.add(key + ".note=" + this.getPropFileValue(entry.note, false));
					lines.add("");
				}
			}
			
			fileUtil.writeLinesToFile(this.getFile(), lines);
			return true;
		}
	}
	
	private String getPropFileValue(String val, boolean encrypt) {
		if (val == null) {
			return "";
		} else {
			if (encrypt) {
				return this.cipher.encrypt(val);
			} else {
				return val;
			}
		}
	}
	
	public String getFile() { return this.file; }
	
	public boolean keyExists(String key) {
		return this.entries.containsKey(key);
	}
	
	public Entry getEntry(String key) {
		return this.entries.get(key);
	}
	
	public Set<String> getKeys() {
		return this.entries.keySet();
	}
	
	public String getUsername(String key) {
		Entry entry = this.getEntry(key);
		
		if (entry == null) {
			logger.warn("An Entry does not exist for key: {}", key);
			return null;
		} else {
			return entry.username;
		}
	}
	
	public String getPassword(String key) {
		Entry entry = this.getEntry(key);
		
		if (entry == null) {
			logger.warn("An Entry does not exist for key: {}", key);
			return null;
		} else {
			return entry.password;
		}
	}
	
	public String getNote(String key) {
		Entry entry = this.getEntry(key);
		
		if (entry == null) {
			logger.warn("An Entry does not exist for key: {}", key);
			return null;
		} else {
			return entry.note;
		}
	}
	
	public boolean addEntry(String key, String username, String password, String note) throws FileSystemException {
		if (this.entries.containsKey(key) == false) {
			this.entries.put(key, new Entry(username, password, note));
			this.save();
			return true;
		} else {
			logger.error("An entry named {} already exists", key);
			return false;
		}
	}
	
	public boolean updateEntry(String key, String username, String password, String note) throws FileSystemException {
		deleteEntry(key);
		return addEntry(key, username, password, note);
	}
	
	public boolean deleteEntry(String key) throws FileSystemException {
		if (this.entries.containsKey(key)) {
			this.entries.remove(key);
			this.save();
			return true;
		} else {
			logger.error("An entry named {} does not exist");
			return false;
		}
	}
	
	public void printKeys() {
		System.out.println("Keys");
		System.out.println("======================");
		if ((this.entries != null) && (this.entries.size() > 0)) {
			for(String key : this.entries.keySet()) {
				System.out.println(key);
			}
		}
	}
	
	public void printKey(String key) {
		Entry entry = getEntry(key);
		System.out.println("");
		System.out.println("Key = " + key);
		System.out.println("Username = " + entry.username);
		System.out.println("Note = " + entry.note);
	}
	
	private class Entry {
		private String username;
		private String password;
		private String note;
		
		protected Entry(String username, String password, String note) {
			this.username = username;
			this.password = password;
			this.note = note;
		}
	}
}
